broApp.service('UserSvc', function ($http) {
    var currentUser;
    return {
        getCurrentUser: function (callback) {
            var config = {
                method: 'GET',
                url: '/currentUser'
            };
            if (currentUser) {
                callback(currentUser);
            } else {
                $http(config)
                    .success(function (user) {
                        currentUser = user;
                        callback(user);
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data, status, headers, config);
                    });
            }
        },
        getUserSessions: function (user, callback) {
            var config = {
                method: 'GET',
                url: '/userSessions/' + user
            };

            $http(config)
                .success(function (sessions) {
                    callback(sessions);
                })
                .error(function (data, status, headers, config) {
                    console.log(data, status, headers, config);
                });

        },
        changePass: function (user, newPass, callback) {
            var config = {
                method: 'POST',
                url: '/changePass',
                data: {
                    user: user,
                    pass: newPass
                }
            };

            $http(config)
                .success(function () {
                    callback();
                })
                .error(function (data, status, headers, config) {
                    console.log(data, status, headers, config);
                });

        },
        logout : function () {
            var config = {
                method: 'GET',
                url: '/logout'
            };

            $http(config)
                .success(function () {
                    window.location.href = '/';
                })
                .error(function (data, status, headers, config) {
                    console.log(data, status, headers, config);
                });
        },
        home : function () {
            var config = {
                method: 'POST',
                url: '/private/'
            };

            $http(config)
                .success(function () {
                    window.location.href = '/private/';
                })
                .error(function (data, status, headers, config) {
                    console.log(data, status, headers, config);
                });
        },
        getCurrentLayout : function (user, callback) {
            var config = {
                method: 'POST',
                url: '/getLang',
                data : {
                    user: user
                }
            };

            $http(config)
                .success(function (langData) {
                    callback(langData);
                })
                .error(function (data, status, headers, config) {
                    console.log(data, status, headers, config);
                    callback({lang:"en",currency:""});
                });
        },
        setCurrentLayout : function (user_id, currency) {
            var config = {
                method: 'POST',
                url: '/setLang',
                data : {
                    Id: user_id,
                    Lang: currency
                }
            };

            $http(config)
                .success(function (langData) {
                })
                .error(function (data, status, headers, config) {
                    console.log(data, status, headers, config);
                });
        }
    };
});
