/**
 * Created by Marc.Rivelles on 07/12/2015.
 */
broApp.service('CalcSvc', function ($http) {
    return {
        optimise: function (channels, overall, callback) {
            var config = {
                method: 'POST',
                url: '/optimise',
                data: {
                    Input: channels,
                    Overall: overall,
                }
            };
            $http(config)
                .success(function (new_data) {
                    callback(new_data);
                })
                .error(function (data, status, headers, config) {
                    console.log(data, status, headers, config);
                });

        },
        curves: function (channels, fit_out, fncs, max_spend, tot_spend, num_increments, num_points, min_spend, callback) {
            var config = {
                method: 'POST',
                url: '/curves',
                data: {
                    Input: channels,
                    OutputFit: fit_out,
                    Fncs: fncs,
                    Total_spend: tot_spend,
                    Max_spend: max_spend,
                    Min_spend: min_spend,
                    Num_incr: num_increments,
                    Num_points: num_points
                }
            };
            $http(config)
                .success(function (new_data) {
                    callback(new_data);
                })
                .error(function (data, status, headers, config) {
                    console.log(data, status, headers, config);
                });

        },
    }
});