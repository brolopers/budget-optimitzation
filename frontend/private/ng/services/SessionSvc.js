broApp.service('SessionSvc', function ($http) {
    var sessionObject = {
        user_details: {}
    };
    var localeData = {
        "name" : "pound",
        "date" : "dd-M-YY",
        "lang" : "en-gb",
        "round" : "0,0",
        "full" : "$0,0.00",
        "simple" : "0,00.00",
        "currPos" : 0
    };
    var current_slide = "welcome";

    return {
        currentSlide: current_slide,
        currentSession: sessionObject,
        locale: localeData,

        loadSession: function (sessionID, callback) {
            var config = {
                method: 'GET',
                url: '/session/' + sessionID
            };
            $http(config)
                .success(function (data) {
                    callback(data);
                })
                .error(function (data, status, headers, config) {
                    console.log(data, status, headers, config);
                });
        },
        saveSession: function (sessionObj, callback) {
            var config = {
                method: 'POST',
                url: '/saveSession',
                data: sessionObj
            };
            $http(config)
                .success(function (data) {
                    callback(data);
                })
                .error(function (data, status, headers, config) {
                    console.log(data, status, headers, config);
                });
        },
        saveSessionAs: function (sessionObj, callback) {
            var config = {
                method: "POST",
                url: "/saveSessionAs",
                data: sessionObj
            };
            $http(config)
                .success(function (data) {
                    callback(data);
                })
                .error(function (data, status, headers, config) {
                    console.log(data, status, headers, config);
                });
        },
        deleteSession: function (sessionObj, callback) {
            var config = {
                method: 'POST',
                url: '/deleteSession',
                data: {
                    Id: sessionObj["id"]
                }
            };
            $http(config)
                .success(function (data) {
                    callback(data);
                })
                .error(function (data, status, headers, config) {
                    console.log(data,status,headers,config);
                });
        },
        deleteAllSessions: function (obj, callback) {
            var config = {
                method: 'POST',
                url: '/delAllSessions',
                data:  {
                    Ids: obj
                }
            };
            $http(config)
                .success(function () {
                    callback();
                })
                .error(function (data, status, headers, config) {
                    console.log(data, status, headers, config);
                });
        }
    };
});