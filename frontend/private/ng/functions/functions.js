/**
 * Created by marc.rivelles on 30/01/14.
 */
/**
 * Api to Create Floatting Table Header
 * @param idTable
 */
function MakeStaticHeader(gridId, height, width, headerHeight, isFooter, divHeader, divMain, divFooter) {
    var tbl = document.getElementById(gridId);
    if (tbl) {
        var DivHR = document.getElementById(divHeader);
        var DivMC = document.getElementById(divMain);
        var DivFR = document.getElementById(divFooter);

        //*** Set divheaderRow Properties ****
        DivHR.style.height = headerHeight + 'px';
        DivHR.style.width = (parseInt(width) - 16) + 'px';
        DivHR.style.position = 'relative';
        DivHR.style.top = '0px';
        DivHR.style.zIndex = '10';
        DivHR.style.verticalAlign = 'top';

        //*** Set divMainContent Properties ****
        DivMC.style.width = width + 'px';
        DivMC.style.height = height + 'px';
        DivMC.style.position = 'relative';
        DivMC.style.top = -headerHeight + 'px';
        DivMC.style.zIndex = '1';

        //*** Set divFooterRow Properties ****
        DivFR.style.width = (parseInt(width) - 16) + 'px';
        DivFR.style.position = 'relative';
        DivFR.style.top = -headerHeight + 'px';
        DivFR.style.verticalAlign = 'top';
        DivFR.style.paddingtop = '2px';

        if (isFooter) {
            var tblfr = tbl.cloneNode(true);
            tblfr.removeChild(tblfr.getElementsByTagName('tbody')[0]);
            var tblBody = document.createElement('tbody');
            tblfr.style.width = '100%';
            tblfr.cellSpacing = "0";
            //*****In the case of Footer Row *******
            tblBody.appendChild(tbl.rows[tbl.rows.length - 1]);
            tblfr.appendChild(tblBody);
            DivFR.appendChild(tblfr);
        }
        //****Copy Header in divHeaderRow****
        DivHR.appendChild(tbl.cloneNode(true));
    }
}

function OnScrollDiv(Scrollablediv, divHeader, divFooter) {
    document.getElementById(divHeader).scrollLeft = Scrollablediv.scrollLeft;
    document.getElementById(divFooter).scrollLeft = Scrollablediv.scrollLeft;
}

var month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
/**
 * Generate Export CSV
 * @param planName
 * @param cols
 * @param rows
 * @param data
 * @param username
 * @param anchor
 */
var exportTableCsv = function(sesName, cols, data, username, anchor) {
    var now = new Date(),
        fileCsv = 'Export Date:,' +
            ((now.getDate() < 10 ? '0' + now.getDate() : now.getDate()) + '-' + month_names[now.getMonth()])  +
            '-' + now.getFullYear().toString().substring(2, 4) + ',,,Session:,' + sesName + ',,,User:,' + username + '\n\n,,';

    fileCsv += cols + '\n';

    for(var i=0; i<data.length;i++){
        fileCsv += ',,' + data[i] + '\n';
    }
    document.getElementById(anchor).download = sesName + "_table_export.csv";
    createExportCsv(fileCsv, anchor);
}

var exportSessionCsv = function(session, table_data, anchor) {
    var fileCsv = ["Channels","Method","Base Plan Spend","Optimal Spend","Min","Max","Threshold","Visible","Number Spends"] + ',\r\n';
    var line = "",
        counter = 0;
    for(var i=0;i<session.curves_table.length;i++) {
        line = session.curves_table[i].Name + "," +
            session.curves_table[i].Function + "," +
            session.curves_in[i].Raw_spend + ",";

        if(session.curves_table[i].Visible == 1){
            line += table_data[counter][2] + "," +
                table_data[counter][6] + "," +
                ([""," "].indexOf(table_data[counter][7]) == -1 ? session.curves_table[i].Max : "") + "," +
                table_data[counter][8] + ",";

            counter++;
        }else{
            line += session.curves_table[i].Spend + "," +
                session.curves_table[i].Min + "," +
                (session.curves_table[i].Max == -1 ? "" : session.curves_table[i].Max) + "," +
                session.curves_table[i].Threshold + ",";
        }
        line += session.curves_table[i].Visible + "," +
            session.curves_in[i].RawX.length + "," +
            session.curves_in[i].RawX + "," +
            session.curves_in[i].RawY + "\r\n";

        fileCsv += line;
    }

    document.getElementById(anchor).download = session.config_name + "_session.csv";
    createExportCsv(fileCsv, anchor);
}

var exportCurvesCsv = function(sesName, data, username, anchor) {
    var now = new Date(),
        fileCsv = 'Export Date:,' +
            ((now.getDate() < 10 ? '0' + now.getDate() : now.getDate()) + '-' + month_names[now.getMonth()])  +
            '-' + now.getFullYear().toString().substring(2, 4) + ',,,Session:,' + sesName + ',,,User:,' + username + '\n\n';

    for(var i=0; i<data.length;i++){
        fileCsv += ',,' + data[i] + '\n';
    }

    createExportCsv(fileCsv, anchor);
}

/**
 * Attach CSV file to download
 * @param csvData
 */
var createExportCsv = function(csvData, anchor){
    var blobdata = new Blob([csvData],{encoding:"UTF-8",type : 'text/csv;charset=UTF-8'});
    if(document.getElementById(anchor) != null) {
        document.getElementById(anchor).href = window.URL.createObjectURL(blobdata);
    }
}

var update_color = {};

var alpha = 0.5;

var gtColors = ["fab200", "ff318a", "cb1584", "accf15", "1fb8ff", "facd00", "f78000", "d4115a", "821a80", "4fa717", "007be8"];

/**
 * Generate Random Colors
 * @returns {string}
 */
var getRandomColor = function() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '';//'#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

/**
 * Convert HEX to RGB text
 * @param hex
 * @returns {string}
 */
var hexToRgb = function(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? "rgb("+
    parseInt(result[1], 16)+","+
    parseInt(result[2], 16)+","+
    parseInt(result[3], 16)+")"
        : null;
}

/************************************************************************
 * Export Tools CSV
 ***********************************************************************/
var base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))); };

/**
 * Attach CSV file to download
 * @param csvData
 */
var createExportCsv = function(csvData, anchor){
    var blobdata = new Blob([csvData],{encoding:"UTF-8",type : 'text/csv;charset=UTF-8'});
    if(document.getElementById(anchor) != null) {
        document.getElementById(anchor).href = window.URL.createObjectURL(blobdata);
    }
}