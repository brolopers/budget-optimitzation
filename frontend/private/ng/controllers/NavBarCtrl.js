var currency = "$", langNumeral = "en", langss = ["en","en-gb","de"], nameCurr = ["Dollar","Pound","Euro"], index_units={};
var app_name = "BOP";

broApp.controller('NavBarCtrl', function($scope, $location, UserSvc, SessionSvc) {
    $scope.AppName = app_name;
    $scope.currencies = ["Dollar","Pound","Euro"];
    $scope.currencySel = $scope.currencies[0];

    $scope.show_chg_currency = function(){
        return SessionSvc.currentSlide == "welcome";
    }

    $scope.openChangeLang = function(){
        //Open Modal
        $('#openAsModalLang').modal("show");
    }
    $scope.updateCurrency = function(){
        var e = document.getElementById("selectLang");
        index_units = e.selectedIndex;
        $scope.currencySel = $scope.currencies[e.selectedIndex];
        var lang = langss[nameCurr.indexOf($scope.currencySel)];
        langNumeral = lang;
        numeral.language(lang);
        var dataC = numeral.languageData(lang);
        currency = dataC.currency.symbol;
        UserSvc.setCurrentLayout($scope.session.user_details.id, lang);
    }
    $scope.goHome = function(){
        bootbox.confirm("Do you want to go Home ?", function(result) {
            if (result) {
                $location.path('/');
                $scope.$apply();
            }
        });
    }
    $scope.goExit = function(){
        bootbox.confirm("Do you want to Exit ?", function(result) {
            if (result) {
                UserSvc.logout();
            }
        });
    }

    /**
     *  INIT
     **/

    UserSvc.getCurrentUser(function(user){
        if(user.lang_web != "" || user.lang != ""){
            if(user.lang_web.slice(0,2) == "en"){
                if(user.lang_web.toLowerCase() == "en-us"){
                    user.lang = "en";
                }else{
                    user.lang = "en-gb";
                }
            }
            if(user.lang != ""){
                user.lang_web = user.lang;
            }
            langNumeral = user.lang;
            numeral.language(user.lang);
            var dataC = numeral.languageData(user.lang);
            currency = dataC.currency.symbol;
        }else{
            langNumeral = "en";
            numeral.language(langNumeral);
            var dataC = numeral.languageData(user.lang);
            currency = dataC.currency.symbol;
        }
        if(langss.indexOf(user.lang) != -1){
            index_units = langss.indexOf(user.lang);
            $scope.currencySel = $scope.currencies[langss.indexOf(user.lang)];
        }else{
            index_units = 2;
            $scope.currencySel = $scope.currencies[2];
        }

        SessionSvc.currentSession.user_details = user;
        $scope.session = SessionSvc.currentSession;
    });
});

