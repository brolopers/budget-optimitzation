﻿broApp.controller('WelcomeCtrl', function($scope, $location, $upload, $timeout, UserSvc, SessionSvc) {
    $scope.AppName = app_name;
    $scope.sessions = [];
    $scope.showSessions = true;
    $scope.showNewSession = false;
    $scope.textLoading = "Loading...";
    $scope.showLoading = false;
    $scope.empty_sessions = true;


    $scope.SessionsVisible = function(){
        $('#manageSessionsBtn').css('border','2px solid #831A81');
        $('#manageConfigsBtn').css('border','2px solid #398439');
        $scope.showNewSession = false;
        $scope.showSessions = true;
    }

    $scope.NewVisible = function(){
        $('#manageSessionsBtn').css('border','2px solid #265a88');
        $('#manageConfigsBtn').css('border','2px solid #831A81');
        $scope.showSessions = false;
        $scope.showNewSession = true;
    }


    /**
     * New session Functions
     */
    /*Upload File*/
    $scope.onFileSelect = function($files) {
        var $file = $files[0];
        bootbox.prompt("New config name for '" + $file.name + "' :", function(result) {
            if (result != null && result != '') {
                 //Show Uploading
                 $scope.textLoading = 'Uploading...';
                 $scope.showLoading = true;
                 //TODO USER PERCENTATGE
                 $upload.upload({
                             url:    '/upDataConfig',
                             method: 'POST',
                             file:   $file,
                             data:   {
                                 confName: result,
                                 username: $scope.session.user_details.username
                            },
                        progress: function (e) {
                            //console.log('percent: ' + e + parseInt(100.0 * e.loaded / e.total));
                            //TODO Change Percentatge
                        }
                 }).success(function (session) {
                     //Saved
                     $scope.session.currentSession = session;
                     SessionSvc.currentSession.data = session;
                     $timeout(function(){$scope.showLoading=false;$scope.savedSuccessfully = true;}, 200);
                     $location.path('/main');
                 }).error(function(){
                    bootbox.dialog({
                         message: "Error Uploading File '" + $file.name + "', check the format.",
                         title: "Upload Error",
                         buttons: {
                             danger: {
                                 label: "Ok, I will check this",
                                 className: "btn-danger"
                             }
                         }
                    });
                    $timeout(function(){$scope.showLoading=false;}, 200);
                 });
            }
        });
    }

    /*Upload Session*/
    $scope.onFileSelect2 = function($files) {
        var $file = $files[0];
        bootbox.prompt("New config name for '" + $file.name + "' :", function(result) {
            if (result != null && result != '') {
                //Show Uploading
                $scope.textLoading = 'Uploading...';
                $scope.showLoading = true;
                //TODO USER PERCENTATGE
                $upload.upload({
                    url:    '/upDataConfig2',
                    method: 'POST',
                    file:   $file,
                    data:   {
                        confName: result,
                        username: $scope.session.user_details.username
                    },
                    progress: function (e) {
                        console.log('percent: ' + e + parseInt(100.0 * e.loaded / e.total));
                        //TODO Change Percentatge
                    }
                }).success(function (session) {
                    //Saved
                    $scope.session.currentSession = session;
                    SessionSvc.currentSession.data = session;
                    $timeout(function(){$scope.showLoading=false;$scope.savedSuccessfully = true;}, 200);
                    $location.path('/main');
                }).error(function(){
                    bootbox.dialog({
                        message: "Error Uploading File '" + $file.name + "', check the format.\nPlease ensure that you are trying to load a session file and not a template/curve file.",
                        title: "Upload Error",
                        buttons: {
                            danger: {
                                label: "Ok, I will check this",
                                className: "btn-danger"
                            }
                        }
                    });
                    $timeout(function(){$scope.showLoading=false;}, 200);
                });
            }
        });
    }

    /**
     * INIT
     */
    SessionSvc.currentSlide = "welcome";
    $scope.session = {
        currentSession: SessionSvc.currentSession.data,
        user_details: SessionSvc.currentSession.user_details
    };
    $scope.SessionsVisible();
});