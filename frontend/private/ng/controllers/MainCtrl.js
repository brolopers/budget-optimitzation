/**
 * Created by Marc.Rivelles on 27/11/2015.
 */
broApp.controller('MainCtrl', function($scope, $timeout, $location, UserSvc, SessionSvc, CalcSvc, $sce) {
    var divTable2 = "tableOVresults2",
        divHeader2 = "DivHeaderRow2",
        divMain2 = "DivMainContent2",
        divFooter2 = "DivFooterRow2";

    var margin = (50 + 100 + 90),
        h_table = 0,
        h_chart = 0,
        w_table = 0,
        w_chart_half = 0,
        hnd = {};

    var headers = ['Channel', 'Base Plan Spend  (' + currency + ')',  'Optimal Spend  (' + currency + ')', 'Difference  (' + currency + ')', 'Sales (' + currency + ')', 'Profit (' + currency + ')', 'Min (' + currency + ')', 'Max (' + currency + ')', 'Threshold (' + currency + ')'];
    var chart_name1 = 'cht01',
        chart_name2 = 'cht02',
        chart_name3 = 'cht03',
        chart_name4 = 'cht04',
        chart1 = {},
        chart2 = {},
        chart3 = {},
        chart4 = {},
        idx_view = [];
    var loaded = false;

    $scope.show_tab0 = true;
    $scope.show_tab1 = false;
    $scope.show_tab2 = false;
    $scope.lock_max_spend = false;
    $scope.showLoading = false;
    $scope.textLoading = "Loading...";
    $scope.savedSuccessfully = false;
    $scope.total_profit = 0;
    $scope.total_sales = 0;
    $scope.total_spend = 0;
    $scope.sel_tab = 0;
    $scope.curve_data = [];
    $scope.sessions = [];
    $scope.opt_area = "spend_cent";
    $scope.o_spend_label = "Optimal Spend";

    $scope.sett = {
        num_points: 30,
        num_increments: 30,
        max_spend: 0,
        mpp: 0
    };

    $scope.unitsList = ["Full Number", "Auto Format", "K (Thousands)", "M (Millions)"];
    $scope.unitsSel = $scope.unitsList[1];

    $scope.export_chart = function(id_chart, img_name){
        saveSvgAsPng(document.getElementById(id_chart).children[0], img_name + ".png", {scale: 5.0});
    };

    update_color = function(color, idx){
        console.log(color, idx);
        $scope.session.data.curves_table[idx.slice(3)].Color = color;
        $scope.$apply();
    }

    $scope.apply_changes = function(){
        var total = 0;
        $scope.session.data.curves_table = $scope.session.data.curves_table.map(function(user){
            user.Visible = (user.chk ? 1 : 0);
            return user;
        });
        for(var i=0;i<$scope.session.data.curves_table.length;i++){
            if($scope.session.data.curves_table[i].Max > -1){
                total += $scope.session.data.curves_table[i].Max;
            }
        }
        var changes_on = true;
        if($scope.sett.num_points == $scope.bck.num_points && $scope.sett.num_increments == $scope.bck.num_increments &&
            $scope.sett.max_spend == numeral().unformat($scope.bck.max_spend) && $scope.sett.mpp == $scope.bck.mpp){
            changes_on = false;
        }
        $scope.sett = {
            num_points: isNaN($scope.bck.num_points) ? 30 : parseInt($scope.bck.num_points),
            num_increments: isNaN($scope.bck.num_increments) ? 30 : parseInt($scope.bck.num_increments),
            max_spend: $scope.bck.mpp ? $scope.sett.max_spend : ($scope.bck.max_spend == "" ? 0.0 : (isNaN(numeral().unformat($scope.bck.max_spend)) ? total * 2 : (numeral().unformat($scope.bck.max_spend) > 0 ? parseFloat(numeral().unformat($scope.bck.max_spend)) : total * 2))),
            mpp: ($scope.bck.mpp ? 1 : 0)
        };
        if(changes_on){
            $scope.optimise();
        }else{
            getCurves(function(){
                switch($scope.sel_tab) {
                    case 0://MAIN
                        $scope.draw_curves();
                        $scope.draw_allocation();
                        break;
                    case 1://Profit Curves
                        $scope.draw_profit_curve();
                        break;
                    case 2://Budget
                        $scope.draw_area();
                        break;
                }
                $timeout(function(){$scope.showLoading=false;}, 100);
            });
        }
    }

    $scope.checkBoxes2 = function(checked){
        var chk = checked=="yes"?true:false;
        $scope.session.data.curves_table = $scope.session.data.curves_table.map(function(user){
            user.chk = chk;
            return user;
        });
    }

    $scope.chk_mpp = function(){
        $scope.lock_max_spend = !$scope.lock_max_spend;
    }

    var convert_chart_value = function(d){
        switch($scope.unitsList.indexOf($scope.unitsSel)) {
            case 0://All Number
                return numeral(d).format("$0,0");
                break
            case 1://Auto
                return numeral(d).format("$0,0.0a");
                break;
            case 2://K
                return (currency == "�" ? (numeral(d).format("0,0") + "k " + currency) : (currency + numeral(d).format("0,0") + "k"));
                break;
            case 3://M
                return (currency == "�" ? (numeral(d).format("0,0") + "M " + currency) : (currency + numeral(d).format("0,0") + "M"));
                break;
        }
    }

    $scope.updateUnits = function(){
        var e = document.getElementById("selectUnits");;
        $scope.unitsSel = $scope.unitsList[e.selectedIndex];

        //Update headers
        headers = ['Channel', 'Base Plan Spend  (' + currency + getUSel() + ')',  'Optimal Spend  (' + currency + getUSel() + ')', 'Difference  (' + currency + getUSel() + ')', 'Sales (' + currency + getUSel() + ')', 'Profit (' + currency + getUSel() + ')', 'Min (' + currency + getUSel() + ')', 'Max (' + currency + getUSel() + ')', 'Threshold (' + currency + getUSel() + ')'];
        if($(window).width() <= 1200){
            headers[1] = 'BP Spend  (' + currency + getUSel() + ')';
            headers[2] = 'O Spend  (' + currency + getUSel() + ')';
            headers[3] = 'Difference  (' + currency + getUSel() + ')';
            headers[8] = 'Threshold (' + currency + getUSel() + ')';

            if($(window).width() <= 1100) {
                headers[3] = 'Diff  (' + currency + getUSel() + ')';
                headers[8] = 'TH (' + currency + getUSel() + ')';
            }
        }
        hnd.updateSettings({colHeaders: headers});
        $scope.extra_units = $("<div />").html(["$", "&#163;", "&euro;"][index_units]).text() + getUSel();
        updateProfit();
        switch($scope.sel_tab) {
            case 0://MAIN
                $scope.draw_curves();
                $scope.draw_allocation();
                break;
            case 1://Profit Curves
                $scope.draw_profit_curve();
                break;
            case 2://Budget
                $scope.draw_area();
                break;
        }
    }

    var default_settings = function(){
        $scope.sett = {
            num_points: $scope.session.data.num_points,
            num_increments: $scope.session.data.num_incr,
            max_spend: $scope.session.data.max_spend,
            mpp: $scope.session.data.middle_pp
        };
    }

    $scope.change_tab = function(tab){
        $scope.show_tab0 = false;
        $scope.show_tab1 = false;
        $scope.show_tab2 = false;
        $('#tab0').removeClass('active');
        $('#tab1').removeClass('active');
        $('#tab2').removeClass('active');
        $scope.sel_tab = tab;
        switch(tab) {
            case 0://MAIN
                $scope.show_tab0 = true;
                $('#tab0').addClass('active');
                $scope.draw_curves();
                $scope.draw_allocation();
                break;
            case 1://Profit Curves
                $scope.show_tab1 = true;
                $('#tab1').addClass('active');
                $scope.draw_profit_curve();
                break;
            case 2://Budget
                $scope.show_tab2 = true;
                $('#tab2').addClass('active');
                $scope.draw_area();
                break;
            default://MAIN
                $scope.show_tab0 = true;
                $('#tab0').addClass('active');
                break;
        }
    }

    var updateProfit = function(){
        $scope.total_profit = 0;
        $scope.total_sales = 0;
        $scope.total_spend = 0;
        $scope.base_spend = 0;
        for(var i=0; i<$scope.session.data.curves_table.length; i++){
            if($scope.session.data.curves_table[i].Visible == 1) {
                $scope.total_profit += $scope.session.data.curves_table[i].Sales - $scope.session.data.curves_table[i].Spend;
                $scope.total_spend += $scope.session.data.curves_table[i].Spend;
                $scope.base_spend += $scope.session.data.curves_in[i].Raw_spend;
                $scope.total_sales += $scope.session.data.curves_table[i].Sales;
            }
        }
        $scope.total_profit = numeral($scope.total_profit).format("$0,0") + getUSel();
        $scope.total_spend = numeral($scope.total_spend).format("$0,0") + getUSel();
        $scope.base_spend = numeral($scope.base_spend).format("$0,0") + getUSel();
        $scope.total_sales = numeral($scope.total_sales).format("$0,0") + getUSel();
    }

    $scope.update_budget_format = function(num){
        num = numeral().unformat(num);
        $scope.session.data.budget = numeral(parseFloat(isNaN(num) ? 0 : (parseFloat(num) >= 0 ? num : 0))).format("0,0.00");
    }

    $scope.chk_num_point = function(num){
        num = parseInt(num);
        $scope.bck.num_points = (isNaN(num) ? 30 : (num >= 5 ? num : 5));
    }

    $scope.chk_num_incr = function(num){
        num = parseInt(num);
        $scope.bck.num_increments = (isNaN(num) ? 30 : (num >= 3 ? num : 3));
    }

    $scope.chk_max_spend = function(num){
        num = numeral().unformat(num);
        var total = 0;
        for(var i=0;i<$scope.session.data.curves_table.length;i++){
            total += $scope.session.data.curves_table[i].Max;
        }
        $scope.bck.max_spend = numeral((num == "" ? 0.0 : (isNaN(parseFloat(num)) ? 30 : (parseFloat(num) > 0 ? parseFloat(num) : total * 2)))).format("0,0.00");
    }

    $scope.create_hnd = function(){
        var data = [];
        idx_view = [];
        for(var i=0;i<$scope.session.data.curves_table.length;i++){
            if($scope.session.data.curves_table[i].Visible == 1) {
                idx_view.push(i);
                data.push({
                    Channel:    $scope.session.data.curves_table[i].Name,
                    BaseSpend:  $scope.session.data.curves_in[i].Raw_spend,
                    Spend:      $scope.session.data.curves_table[i].Spend,
                    Diff:       $scope.session.data.curves_table[i].Spend - $scope.session.data.curves_in[i].Raw_spend,
                    Sales:      $scope.session.data.curves_table[i].Sales,
                    Profit:     $scope.session.data.curves_table[i].Sales - $scope.session.data.curves_table[i].Spend,
                    Min:        $scope.session.data.curves_table[i].Min,
                    Max:        $scope.session.data.curves_table[i].Max == -1 ? "" : $scope.session.data.curves_table[i].Max,
                    Threshold:  $scope.session.data.curves_table[i].Threshold
                });
            }
        }
        var colWidths = [];
        for(var i=0; i<headers.length; i++){
            colWidths.push(w_table/headers.length);
        }

        updateProfit();

        var container = document.getElementById('inputGrid');
        hnd = new Handsontable(container, {
            data: data,
            rowHeaders: false,
            colHeaders: headers,
            contextMenu: false,
            colWidths: colWidths,
            columns: [
                {
                    data: 'Channel',
                    readOnly: true,
                },
                {
                    data: 'BaseSpend',
                    readOnly: true,
                    type: 'numeric',
                    format: '0,0.00',
                    language: langNumeral
                },
                {
                    data: 'Spend',
                    readOnly: true,
                    type: 'numeric',
                    format: '0,0.00',
                    language: langNumeral
                },
                {
                    data: 'Diff',
                    readOnly: true,
                    type: 'numeric',
                    format: '0,0.00',
                    language: langNumeral
                },
                {
                    data: 'Sales',
                    readOnly: true,
                    type: 'numeric',
                    format: '0,0.00',
                    language: langNumeral
                },
                {
                    data: 'Profit',
                    readOnly: true,
                    type: 'numeric',
                    format: '0,0.00',
                    language: langNumeral
                },
                {
                    data: 'Min',
                    type: 'numeric',
                    format: '0,0.00',
                    language: langNumeral
                },
                {
                    data: 'Max',
                    type: 'numeric',
                    format: '0,0.00',
                    language: langNumeral
                },
                {
                    data: 'Threshold',
                    type: 'numeric',
                    format: '0,0.00',
                    language: langNumeral
                },
            ],
            height: function(){get_sizes(); return h_table;},
            //width: function(){get_sizes(); return w_table;},
            beforeChange: function (changes, source) {
                if(["alter", "edit", "populateFromArray", "paste"].indexOf(source) != -1) {
                    for (var x = 0; x < changes.length; x++) {
                        switch (changes[x][1]) {
                            case "Spend":
                                if (changes[x][3] < 0.0) {
                                    changes[x][3] = 0.0;
                                }
                                break;
                            case "Min":
                                if($scope.session.data.curves_table[idx_view[changes[x][0]]].Max == -1){
                                    if(changes[x][3] < 0) {
                                        changes[x][3] = 0.0;
                                    }
                                }else if (changes[x][3] > $scope.session.data.curves_table[idx_view[changes[x][0]]].Max || changes[x][3] < 0) {
                                    changes[x][3] = $scope.session.data.curves_table[idx_view[changes[x][0]]].Max;
                                }
                                break;
                            case "Max":
                                if (changes[x][3] < $scope.session.data.curves_table[idx_view[changes[x][0]]].Min || changes[x][3] < 0) {
                                    changes[x][3] = $scope.session.data.curves_table[idx_view[changes[x][0]]].Min;
                                }
                                break;
                            case "Threshold":
                                if (changes[x][3] < 0) {
                                    changes[x][3] = 0.0;
                                }
                                break;
                            default:
                                break;
                        }

                    }
                }
            },
            afterChange: function(changes, source){
                if(["alter", "edit", "populateFromArray", "paste"].indexOf(source) != -1){
                    for(var x=0;x<changes.length;x++){
                        switch(changes[x][1]){
                            case "Spend":
                                $scope.session.data.curves_table[idx_view[changes[x][0]]].Spend = changes[x][3];
                                var profit = $scope.session.data.curves_table[idx_view[changes[x][0]]].Sales - $scope.session.data.curves_table[idx_view[changes[x][0]]].Spend;
                                hnd.setDataAtCell(changes[x][0],3, profit);
                                break;
                            case "Min":
                                if (changes[x][3] >= 0) {
                                    $scope.session.data.curves_table[idx_view[changes[x][0]]].Min = changes[x][3];
                                }
                                break;
                            case "Max":
                                if (changes[x][3] == "" && changes[x][3] < 0){
                                    $scope.session.data.curves_table[idx_view[changes[x][0]]].Max = -1;
                                }else{
                                    $scope.session.data.curves_table[idx_view[changes[x][0]]].Max = changes[x][3];
                                }
                                break;
                            case "Threshold":
                                if (changes[x][3] >= 0) {
                                    $scope.session.data.curves_table[idx_view[changes[x][0]]].Threshold = changes[x][3];
                                }
                                break;
                            default:
                                break;
                        }

                    }
                    updateProfit();
                    //Draw...
                    switch($scope.sel_tab) {
                        case 0://MAIN
                            $scope.draw_curves();
                            $scope.draw_allocation();
                            break;
                        case 1://Profit Curves
                            $scope.draw_profit_curve();
                            break;
                        case 2://Budget
                            $scope.draw_area();
                            break;
                    }
                    $scope.$apply();
                    exportTableCsv($scope.session.data.config_name, headers, hnd.getData(), $scope.session.user_details.username, "linkExportMainData");
                    exportSessionCsv($scope.session.data, hnd.getData(), "linkExportMainData2");
                }
            }
        });
        exportTableCsv($scope.session.data.config_name, headers, hnd.getData(), $scope.session.user_details.username, "linkExportMainData");
        exportSessionCsv($scope.session.data, hnd.getData(), "linkExportMainData2");
    }

    $scope.draw_curves = function(){
        //Draw Curves Sales Y axis - Spend X axis
        var data = [];
        var c_color = 0;
        var newSize = $scope.curve_data.Sales.length;
        var values_base = Array.apply(null, new Array(newSize)).map(Number.prototype.valueOf, 0);
        var exp_values = [Array.apply(null, new Array(newSize + 1)).map(String.prototype.valueOf, "")];
        exp_values[0][Math.floor(newSize/2)] = "Actual Point";
        exp_values[0][0] = "Channel";
        exp_values[0][1] = "Metric";

        var counter = 0;
        for(var i=0;i<$scope.session.data.curves_table.length;i++) {
            if ($scope.session.data.curves_table[i].Visible == 1) {
                exp_values.push([$scope.session.data.curves_table[i].Name, "Spend"].concat($scope.curve_data.Spend.map(function(x){ return x[counter]; })));
                exp_values.push([$scope.session.data.curves_table[i].Name, "Sales"].concat($scope.curve_data.Sales.map(function(x){ return x[counter]; })));
                var color = $scope.session.data.curves_table[i].Color;
                if (color == "" || typeof color == "undefined") {
                    color = getColorGT(c_color);
                    $scope.session.data.curves_table[i].Color = color;
                    c_color++;
                }
                var values = values_base.map(function(value, index){
                    if(index > 1 && $scope.curve_data.Spend[index][counter] <= 0 && $scope.curve_data.Spend[index][counter] >= 0){
                        return {x:$scope.curve_data.Spend[index][counter], y:null};
                    }else{
                        return {x:$scope.curve_data.Spend[index][counter], y:$scope.curve_data.Sales[index][counter]};
                    }
                });
                data.push({
                    key:        $scope.session.data.curves_table[i].Name,
                    type_ch:    "line",
                    typeLegend: "line",
                    color:      hexToRgb(color),
                    pointIdx:   [Math.floor(newSize/2)],
                    yAxis:      1,
                    values:     values,
                });
                counter++;
            }
        }
        exportCurvesCsv($scope.session.data.config_name, exp_values, $scope.session.user_details.username, "linkExportCurves");

        nv.addGraph(function () {
            chart1 = nv.models.lineChart()
                .options({
                    transitionDuration: 0,
                    toolBar: true,
                    idChart: '#' + chart_name1,
                    margin: {top: 40, right: 40, bottom: 40, left: 70},
                    headTitle: "Curves",
                    showLegend: data.length <= 9,
                    tooltips: true,
                    draw_points: true,
                    tooltipContent: function (key, y, e, graph) {
                        var content = '<h3 style="background-color: ';
                        content += graph.series.color + ';color: #FFFFFF;font: bold 14px BrownStd-Bold">';
                        content += key + '</h3><p style="font-weight:bold;text-align:left;display:inline;padding-right:0px;">Spend: </p><p style="text-align:left;display:inline;">' + convert_chart_value(graph.point.x) + '</p>';
                        content += '<p style="font-weight:bold;text-align:left;display:inline;padding-right:0px;">Sales: </p><p style="text-align:left;display:inline;"> ' + convert_chart_value(graph.point.y) + '</p>';
                        return content;
                    }
                });

            chart1.xAxis
                .axisLabel("Spend (" + currency + getUSel() + ")")
                .tickFormat(function (d) {
                    return convert_chart_value(d);
                });

            chart1.yAxis
                .axisLabel("Sales (" + currency + getUSel() + ")")
                .tickFormat(function (d) {
                    return convert_chart_value(d);
                });

            d3.select("#" + chart_name1 + " svg")
                .datum(data)
                .call(chart1);

            nv.utils.windowResize(function() {
                if($scope.sel_tab == 0){
                    get_sizes();
                    chart1.update();
                }
            });
            return chart1;
        });
    }

    var getColorGT = function(num){
        if(num >= gtColors.length){
            return getRandomColor();
        }else{
            return gtColors[num];
        }
    }

    var getUSel = function(){
        return ($scope.unitsList.indexOf($scope.unitsSel)==2 ? "k" : ($scope.unitsList.indexOf($scope.unitsSel)==3 ? "M" : ""));
    }

    $scope.draw_allocation = function(){
        //Draw Barchart Spend Y axis - Channel X axis
        var data = [];
        var c_color = 0;
        var exp_values = [["Channel", "Spend"]];

        for(var i=0;i<$scope.session.data.curves_table.length;i++) {
            if ($scope.session.data.curves_table[i].Visible == 1) {
                var color = $scope.session.data.curves_table[i].Color;
                if (color == "" || typeof color == "undefined") {
                    color = getColorGT(c_color);
                    $scope.session.data.curves_table[i].Color = color;
                    c_color++;
                }
                data.push({
                    key:        $scope.session.data.curves_table[i].Name,
                    type_ch:    "bar",
                    typeLegend: "circle",
                    yAxis:      1,
                    values:     [{
                        x:      "",
                        y:      $scope.session.data.curves_table[i].Spend,
                        series: 0,
                        color:  hexToRgb(color),
                    }],
                    color: hexToRgb(color),
                });
                exp_values.push([$scope.session.data.curves_table[i].Name, $scope.session.data.curves_table[i].Spend]);
            }
        }

        exportCurvesCsv($scope.session.data.config_name, exp_values, $scope.session.user_details.username, "linkExportAlloc");

        var ttip = function (key, y, e, graph) {
            var content = '<table><thead><tr><td colspan="3" class="x-value">' + "";
            content += '</td></tr></thead><tbody>';
            content += '<tr class="highlight"><td class="legend-color-guide"><div style="background-color:' + graph.series.color + ';"></div></td>';
            content += '<td class="key">' + key.split('_$_').join(' ') + '</td><td class="value">' + e + '</td></tr>';
            content += '</tbody></table>';
            return content;
        }
        nv.addGraph(function () {
            d3.select('#' + chart_name2 + ' svg .nv-headTitle').text('');
            chart2 = nv.models.multiBarChart()
                .duration(0)
                .showLegend(data.length <= 9)
                .headTitle("Allocation")
                .idChart('#' + chart_name2)
                .showControls(false)
                .showGrid(true)
                .margin({top: 40, right: 10, bottom: 20, left: 70})
                .groupSpacing(0)
                .rotateLabels(0)
                .reduceXTicks(false)
                .stacked(false)
                .tooltips(true)
                .two_lines(false)
                .split_txt('_$_')
                .vertical_lines(false)
                .tooltipContent(ttip);

            chart2.xAxis
                .axisLabel('');

            chart2.yAxis
                .axisLabel("Spend (" + currency + getUSel() + ")")
                .tickFormat(function (d) {
                    return convert_chart_value(d);
                });

            d3.select("#" + chart_name2 + " svg")
                .datum(data)
                .call(chart2);

            nv.utils.windowResize(function() {
                if($scope.sel_tab == 0){
                    get_sizes();
                    chart2.update();
                }
            });

            return chart2;
        });
    }

    $scope.draw_profit_curve = function(){
        //Draw Sales Y axis - Spend X axis
        //Curve Sales and Curve Profit
        //Draw a line or mark the points where Sales/Profit are the highest
        var draw_stroke = false;
        var data = [
            {
                key:        "Sales",
                type_ch:    "line",
                typeLegend: "line",
                color:      "rgb(0, 130, 255)",
                pointIdx:   [],
                yAxis:      1,
                stroke_line:1,
                values:     [],
            },
            {
                key:        "Profit",
                type_ch:    "line",
                typeLegend: "line",
                color:      "rgb(255, 49, 138)",
                pointIdx:   [],
                yAxis:      1,
                values:     [],
            },
        ];
        var newSize = $scope.curve_data.Profit_sales.length;
        var exp_values = [Array.apply(null, new Array(newSize + 1)).map(String.prototype.valueOf, "")];
        var values_sales = Array.apply(null, new Array(newSize)).map(Number.prototype.valueOf, 0),
            values_profit = Array.apply(null, new Array(newSize)).map(Number.prototype.valueOf, 0),
            values_spend = Array.apply(null, new Array(newSize)).map(Number.prototype.valueOf, 0);
        var counter = 0;
        for(var i=0;i<$scope.session.data.curves_table.length;i++){
            if ($scope.session.data.curves_table[i].Visible == 1) {
                values_sales = values_sales.map(function(value, index){
                    return value + $scope.curve_data.Profit_sales[index][counter];
                });
                values_spend = values_spend.map(function(value, index){
                    return value + $scope.curve_data.Profit_spend[index][counter];
                });
                counter++;
            }
        }

        for(var i=0;i<values_spend.length;i++){
            values_profit[i] = values_sales[i] - values_spend[i];
        }

        exp_values.push(["Spend"].concat(values_spend));
        exp_values.push(["Sales"].concat(values_sales));
        exp_values.push(["Profit"].concat(values_profit));

        var max_profit_point = values_profit.indexOf(Math.max.apply(null, values_profit));

        var all_values = values_profit.concat(values_sales);
        var highpoint_value = Math.max.apply(null, all_values);
        var lowpoint_value = Math.min.apply(null, all_values);

        data[0].stroke_line = 2;
        var val_pos_ready = false;
        for(var i=0; i<values_profit.length; i++ ){
            if(val_pos_ready && values_sales[i] <= 0){
                data[0].values.push({x:values_spend[i], y:null});
                data[1].values.push({x:values_spend[i], y:null});
                break;
            }else{
                data[0].values.push({x:values_spend[i], y:values_sales[i]});
                data[1].values.push({x:values_spend[i], y:values_profit[i]});
                if(values_sales[i] > 0 ){
                    val_pos_ready = true;
                }
            }
        }
        var val_sp_pp = -1;
        if($scope.curve_data.PPoint_spend.length > 0){
            val_sp_pp = $scope.curve_data.PPoint_spend.reduce(function(a,b){return a+b;})
        }

        //if(val_sp_pp == values_spend[max_profit_point]){
        if((1 - values_spend[max_profit_point] / val_sp_pp) < 0.00001 || val_sp_pp < 0.01){
            exp_values[0][max_profit_point + 1] = "Max Profit Point";
            draw_stroke = true;
            data.push(
            {
                key:        "Max Profit Point",
                type_ch:    "line",
                typeLegend: "line",
                color:      "rgb(255, 0, 0)",
                pointIdx:   [],
                yAxis:      1,
                values:     [
                    {x:values_spend[max_profit_point], y:lowpoint_value},
                    {x:values_spend[max_profit_point], y:highpoint_value}
                ]
            });
        }

        exportCurvesCsv($scope.session.data.config_name, exp_values, $scope.session.user_details.username, "linkExportProfit");

        nv.addGraph(function () {
            chart3 = nv.models.lineChart()
                .options({
                    transitionDuration: 0,
                    toolBar: true,
                    idChart: '#' + chart_name3,
                    margin: {top: 40, right: 20, bottom: 10, left: 70},
                    headTitle: "Profit Curve",
                    tooltips: true,
                    legend_bottom: true,
                    draw_points: true,
                    draw_stroke: draw_stroke,
                    tooltipContent: function (key, y, e, graph) {
                        var content = '<h3 style="background-color: ';
                        content += graph.series.color + ';color: #FFFFFF;font: bold 14px BrownStd-Bold">';
                        content += key + '</h3><p style="font-weight:bold;text-align:left;display:inline;padding-right:0px;">Spend: </p><p style="text-align:left;display:inline;">' + convert_chart_value(graph.point.x) + '</p>';
                        content += '<p style="font-weight:bold;text-align:left;display:inline;padding-right:0px;">Amount: </p><p style="text-align:left;display:inline;"> ' + convert_chart_value(graph.point.y) + '</p>';
                        return content;
                    }
                });

            chart3.xAxis
                .axisLabel("Spend (" + currency + getUSel() + ")")
                .tickFormat(function (d) {
                    return convert_chart_value(d);
                });

            chart3.yAxis
                .axisLabel("Amount (" + currency + getUSel() + ")")
                .tickFormat(function (d) {
                    return convert_chart_value(d);
                });

            d3.select("#" + chart_name3 + " svg")
                .datum(data)
                .call(chart3);

            nv.utils.windowResize(function() {
                if($scope.sel_tab == 1){
                    get_sizes();
                    chart3.update();
                }
            });

            return chart3;
        });

    }

    $scope.draw_area = function(){
        //Draw Area Chart Allocation % is  Y axis
        var data = [];
        var c_color = 0;
        var newSize = $scope.curve_data.Area_data.length;
        var values_base = Array.apply(null, new Array(newSize)).map(Number.prototype.valueOf, 0);
        var exp_values = [Array.apply(null, new Array(newSize + 1)).map(String.prototype.valueOf, "")];

        var draw_stroke = false;
        var newSize = $scope.curve_data.Profit_sales.length;
        var values_sales = Array.apply(null, new Array(newSize)).map(Number.prototype.valueOf, 0),
            values_profit = Array.apply(null, new Array(newSize)).map(Number.prototype.valueOf, 0),
            values_spend = Array.apply(null, new Array(newSize)).map(Number.prototype.valueOf, 0);

        exp_values.push(["Spend"].concat($scope.curve_data.Area_data.map(function(x){ return x.OverallSpend;})));
        exp_values.push(["Channel"]);

        var counter = 0;

        for(var i=0;i<$scope.session.data.curves_table.length;i++) {
            if ($scope.session.data.curves_table[i].Visible == 1) {
                var color = $scope.session.data.curves_table[i].Color;
                if (color == "" || typeof color == "undefined") {
                    color = getColorGT(c_color);
                    $scope.session.data.curves_table[i].Color = color;
                    c_color++;
                }
                var values = [];
                values_base.map(function(value, index){
                    if($scope.curve_data.Area_data[index].Spends){

                        switch ($scope.opt_area) {
                            case "spend_abs":
                                values.push([$scope.curve_data.Area_data[index].OverallSpend, $scope.curve_data.Area_data[index].Spends[counter]]);
                                break;
                            case "spend_cent":
                                values.push([$scope.curve_data.Area_data[index].OverallSpend, $scope.curve_data.Area_data[index].Spends[counter] / $scope.curve_data.Area_data[index].OverallSpend]);
                                break;
                            case "sales_abs":
                                values.push([$scope.curve_data.Area_data[index].OverallSpend, $scope.curve_data.Area_data[index].Sales[counter]]);
                                break;
                            case "sales_cent":
                                values.push([$scope.curve_data.Area_data[index].OverallSpend, $scope.curve_data.Area_data[index].Sales[counter] / $scope.curve_data.Area_data[index].OverallSales]);
                                break;
                        }

                    }
                });
                switch ($scope.opt_area) {
                    case "spend_abs":
                        exp_values.push([$scope.session.data.curves_table[i].Name].concat(values.map(function(value, index){ return $scope.curve_data.Area_data[index].Spends[counter];})));
                        break;
                    case "spend_cent":
                        exp_values.push([$scope.session.data.curves_table[i].Name].concat(values.map(function(value, index){ return $scope.curve_data.Area_data[index].OverallSpend > 0 ? ($scope.curve_data.Area_data[index].Spends[counter]/$scope.curve_data.Area_data[index].OverallSpend) : 0;})));
                        break;
                    case "sales_abs":
                        exp_values.push([$scope.session.data.curves_table[i].Name].concat(values.map(function(value, index){ return $scope.curve_data.Area_data[index].Sales[counter];})));
                        break;
                    case "sales_cent":
                        exp_values.push([$scope.session.data.curves_table[i].Name].concat(values.map(function(value, index){ return $scope.curve_data.Area_data[index].OverallSales > 0 ? ($scope.curve_data.Area_data[index].Sales[counter]/$scope.curve_data.Area_data[index].OverallSales) : 0;})));
                        break;
                }
                data.push({
                    key:        $scope.session.data.curves_table[i].Name,
                    type_ch:    "bar",
                    type:       "area",
                    typeLegend: "circle",
                    color:      hexToRgb(color),
                    yAxis:      1,
                    values:     values,
                });

                values_sales = values_sales.map(function(value, index){
                    return value + $scope.curve_data.Profit_sales[index][counter];
                });
                values_spend = values_spend.map(function(value, index){
                    return value + $scope.curve_data.Profit_spend[index][counter];
                });
                counter++;
            }
        }


        for(var i=0;i<values_spend.length;i++){
            values_profit[i] = values_sales[i] - values_spend[i];
        }

        var max_profit_point = values_profit.indexOf(Math.max.apply(null, values_profit));

        var val_sp_pp = -1;
        if($scope.curve_data.PPoint_spend.length > 0){
            val_sp_pp = $scope.curve_data.PPoint_spend.reduce(function(a,b){return a+b;})
        }

        if((1 - values_spend[max_profit_point] / val_sp_pp) < 0.00001 || val_sp_pp < 0.01){
            var top_value = 0;

            switch ($scope.opt_area) {
                case "spend_abs":
                    top_value = $scope.curve_data.Area_data[max_profit_point].Spends.reduce(function(a,b){ return a+b});
                    break;
                case "spend_cent":
                    top_value = 0.85;
                    break;
                case "sales_abs":
                    top_value = $scope.curve_data.Area_data[max_profit_point].Sales.reduce(function(a,b){ return a+b});
                    break;
                case "sales_cent":
                    top_value = 0.85;
                    break;
            }

            draw_stroke = true;
            data[0].stroke_line = data.length;

            exp_values[0][max_profit_point + 1] = "Max Profit Point";
            data.push({
                    key:        "Max Profit Point",
                    type_ch:    "bar",
                    type:       "line",
                    typeLegend: "line",
                    color:      "rgb(255, 0, 0)",
                    pointIdx:   [],
                    yAxis:      1,
                    values:     [
                        {x:$scope.curve_data.Area_data[0].OverallSpend, y:null},
                        {x:$scope.curve_data.Area_data[max_profit_point].OverallSpend, y:0},
                        {x:$scope.curve_data.Area_data[max_profit_point].OverallSpend, y:top_value},
                        {x:$scope.curve_data.Area_data[$scope.curve_data.Area_data.length-1].OverallSpend, y:null}
                    ]
                });
        }

        exportCurvesCsv($scope.session.data.config_name, exp_values, $scope.session.user_details.username, "linkExportArea");

        //Chart Options
        var yAxisLabel = "",
            xAxisLabel = "",
            option_style = "";

        switch ($scope.opt_area) {
            case "spend_abs":
                yAxisLabel = "Spend Allocation (" + currency + getUSel() + ")"
                xAxisLabel = "Spend (" + currency + getUSel() + ")";
                option_style = "stack";
                break;
            case "spend_cent":
                yAxisLabel = "% Spend Allocation";
                xAxisLabel = "Spend (" + currency + getUSel() + ")";
                option_style = "expand";
                break;
            case "sales_abs":
                yAxisLabel = "Sales Allocation (" + currency + getUSel() + ")"
                xAxisLabel = "Spend (" + currency + getUSel() + ")";
                option_style = "stack";
                break;
            case "sales_cent":
                yAxisLabel = "% Sales Allocation";
                xAxisLabel = "Spend (" + currency + getUSel() + ")";
                option_style = "expand";
                break;
        }
        d3.selectAll("svg > *").remove();
        nv.addGraph(function() {
            chart4 = nv.models.stackedAreaChart()
                .useInteractiveGuideline(true)
                .showControls(false)
                .showLegend(data.length <= 9)
                .draw_stroke(draw_stroke)
                .idChart('#' + chart_name4)
                .x(function(d) { return d[0] })
                .y(function(d) { return d[1] })
                .style(option_style)
                .headTitle("Budget")
                .color(function(d) { return d.color })
                .yAxisTickFormat(function(d){return numeral(d).format("0,0.0a");})
                .margin({"left":80,"right":20,"top":30,"bottom":70});

            chart4.xAxis
                .axisLabel(xAxisLabel)
                .tickFormat(function (d) {
                    return convert_chart_value(d);
                });

            chart4.yAxis
                .axisLabel(yAxisLabel);

            d3.select("#" + chart_name4 + " svg")
                .datum(data)
                .call(chart4);

            nv.utils.windowResize(function() {
                if($scope.sel_tab == 2){
                    get_sizes();
                    chart4.update();
                }
            });

            return chart4;
        });

    }

    $scope.changeAreaChart = function(opt){
        $scope.opt_area = opt;
        $scope.draw_area();
    }

    var get_sizes = function(){
        //Update headers
        headers = ['Channel', 'Base Plan Spend  (' + currency + getUSel() + ')',  'Optimal Spend  (' + currency + getUSel() + ')', 'Difference  (' + currency + getUSel() + ')', 'Sales (' + currency + getUSel() + ')', 'Profit (' + currency + getUSel() + ')', 'Min (' + currency + getUSel() + ')', 'Max (' + currency + getUSel() + ')', 'Threshold (' + currency + getUSel() + ')'];
        $scope.o_spend_label = "Optimal Spend";
        if($(window).width() <= 1300){
            $scope.o_spend_label = "O Spend";
        }
        if($(window).width() <= 1200){
            headers[1] = 'BP Spend  (' + currency + getUSel() + ')';
            headers[2] = 'O Spend  (' + currency + getUSel() + ')';
            headers[3] = 'Difference  (' + currency + getUSel() + ')';
            headers[8] = 'Threshold (' + currency + getUSel() + ')';

            h_table = ($(window).height()- margin) * 0.40;
            h_chart = (($(window).height()- margin) * 0.65) - 31;
            w_table = $(window).width() - 70;
            w_chart_half = ($(window).width() - 40);

            $('.chart_ext_box_full').css('width',(w_chart_half + 15) + 'px');
            if($(window).width() <= 1100) {
                headers[3] = 'Diff  (' + currency + getUSel() + ')';
                headers[8] = 'TH (' + currency + getUSel() + ')';

                if($(window).width() <= 900){
                    w_table += 40;
                    w_chart_half += 15;
                    $('.chart_ext_box_full').css('width',(w_chart_half + 5) + 'px');
                }else{
                    w_table += 30;
                    w_chart_half += 20;
                    $('.chart_ext_box_full').css('width',(w_chart_half + 5) + 'px');
                }
            }

            $('.chart_ext_box').css('height',h_chart + 'px');
            $('.chart_ext_box').css('width',w_chart_half + 'px');
            $('.chart_ext_box_full').css('height',h_chart + 'px');

            $('.chart_box').css('height',(h_chart - 20) + 'px');
            $('.chart_box').css('width',(w_chart_half) + 'px');
            $('.chart_box_full').css('height',(h_chart - 20) + 'px');
            $('.chart_box_full').css('width',(w_chart_half) + 'px');

        }else{
            headers[1] = 'Base Plan Spend  (' + currency + getUSel() + ')';
            headers[2] = 'Optimal Spend  (' + currency + getUSel() + ')';
            headers[3] = 'Difference  (' + currency + getUSel() + ')';
            headers[8] = 'Threshold (' + currency + getUSel() + ')';

            h_table = ($(window).height()- margin) * 0.40 - (($(window).width() <= 1400) ? 45 : 0);
            h_chart = ($(window).height()- margin) * 0.65;
            w_table = $(window).width() - 60;
            w_chart_half = ($(window).width() - 60) * 0.5;

            $('.chart_ext_box').css('height',h_chart + 'px');
            $('.chart_ext_box').css('width',w_chart_half + 'px');
            $('.chart_ext_box_full').css('height',h_chart + 'px');
            $('.chart_ext_box_full').css('width',((w_chart_half * 2) + 30) + 'px');
            $('.chart_box').css('height',(h_chart - 20) + 'px');
            $('.chart_box').css('width',(w_chart_half - 20) + 'px');
            $('.chart_box_full').css('height',(h_chart - 20) + 'px');
            $('.chart_box_full').css('width',((w_chart_half * 2) + 10) + 'px');
        }
        if(loaded){
            hnd.updateSettings({colHeaders: headers});
        }
    }

    $scope.openSett = function(){
        for(var i=0;i<$scope.session.data.curves_table.length;i++){
            $scope.session.data.curves_table[i].chk = ($scope.session.data.curves_table[i].Visible == 1);
            $scope.session.data.curves_table[i].Residual = numeral($scope.session.data.curves_out[i].Residual).format("0,0.0000000");
        }
        $scope.bck = {
            num_points: $scope.sett.num_points,
            num_increments: $scope.sett.num_increments,
            max_spend: $scope.sett.max_spend == 0 ? "" : numeral($scope.sett.max_spend).format("0,0.00"),
            mpp: ($scope.sett.mpp == 1 ? true : false)
        };

        $scope.lock_max_spend = ($scope.sett.mpp == 1 ? true : false);
        jscolor.register();
        $('#openAsModal2').modal("show");
        var icons = {header: "ui-icon-circle-arrow-e", activeHeader: "ui-icon-circle-arrow-s"};
        $("#mediaDropDown").change();
        $("#accordion").accordion({heightStyle: "content", icons: icons, collapsible: true});
        $("#innerAccord").accordion({heightStyle: "content", collapsible: true});
        $("#innerAccord2").accordion({heightStyle: "content", collapsible: true});
        $("#innerAccord3").accordion({heightStyle: "content", collapsible: true});
        $timeout(function () {resizeStable2();}, 40);
        $timeout(function () {resizeStable2();}, 200);
    }

    var getCurves = function(callback){
        //GET Curves for all channels
        var Channels = [], total_spend = [], fit_out = [], fncs = [], min_spend = 0;
        for(var i=0;i<$scope.session.data.curves_table.length;i++) {
            if ($scope.session.data.curves_table[i].Visible == 1) {
                Channels.push({
                    Function: $scope.session.data.curves_table[i].Function,
                    Name: $scope.session.data.curves_table[i].Name,
                    FittedPars: $scope.session.data.curves_out[i].FittedPars,
                    Min: $scope.session.data.curves_table[i].Min,
                    Max: $scope.session.data.curves_table[i].Max == -1 ? Math.pow(10, 10) : $scope.session.data.curves_table[i].Max,
                    Threshold: $scope.session.data.curves_table[i].Threshold
                });
                total_spend.push($scope.session.data.curves_table[i].Spend);
                fit_out.push($scope.session.data.curves_out[i]);
                fncs.push($scope.session.data.curves_in[i].Function);
                min_spend += $scope.session.data.curves_table[i].Min;
            }
        }
        if(Channels.length > 0) {
            CalcSvc.curves(Channels, fit_out, fncs, ($scope.sett.mpp == 1 ? 0 : $scope.sett.max_spend), total_spend, $scope.sett.num_increments, $scope.sett.num_points, min_spend, function(result) {
                //Array Spend Array Sales
                $scope.curve_data = result;
                callback();
            });
        }else{
            $timeout(function(){$scope.showLoading=false;}, 100);
        }

    }

    $scope.optimise = function(){
        //Call Webservice Optimiser
        var Channels = [];
        for(var i=0;i<$scope.session.data.curves_table.length;i++) {
            if ($scope.session.data.curves_table[i].Visible == 1) {
                Channels.push({
                    Function:   $scope.session.data.curves_table[i].Function,
                    Name:       $scope.session.data.curves_table[i].Name,
                    FittedPars: $scope.session.data.curves_out[i].FittedPars,
                    Min:        $scope.session.data.curves_table[i].Min,
                    Max:        $scope.session.data.curves_table[i].Max == -1 ? Math.pow(10, 10) : $scope.session.data.curves_table[i].Max,
                    Threshold:  $scope.session.data.curves_table[i].Threshold
                });
            }
        }
        if(Channels.length > 0) {
            $scope.textLoading = 'Optimising...';
            $scope.showLoading = true;
            CalcSvc.optimise(Channels, numeral().unformat($scope.session.data.budget), function (result) {
                if(result.msg.toLowerCase() != "ok"){
                    $scope.showLoading = false;
                    /*Show Error*/
                    bootbox.dialog({
                        message: result.msg,
                        title: "Error Optimiser",
                        buttons: {
                            danger: {
                                label: "Ok, I will check",
                                className: "btn-danger"
                            }
                        }
                    });
                }else{
                    //Update Table / Chart
                    var counter = 0;
                    var data = [];
                    idx_view = [];
                    for(var i=0;i<$scope.session.data.curves_table.length;i++){
                        if($scope.session.data.curves_table[i].Visible == 1){
                            $scope.session.data.curves_table[i].Sales = result.sales[counter];
                            $scope.session.data.curves_table[i].Spend = result.spend[counter];
                            counter++;
                            idx_view.push(i);
                            data.push({
                                Channel:    $scope.session.data.curves_table[i].Name,
                                BaseSpend:  $scope.session.data.curves_in[i].Raw_spend,
                                Spend:      $scope.session.data.curves_table[i].Spend,
                                Diff:       $scope.session.data.curves_in[i].Raw_spend - $scope.session.data.curves_table[i].Spend,
                                Sales:      $scope.session.data.curves_table[i].Sales,
                                Profit:     $scope.session.data.curves_table[i].Sales - $scope.session.data.curves_table[i].Spend,
                                Min:        $scope.session.data.curves_table[i].Min,
                                Max:        $scope.session.data.curves_table[i].Max == -1 ? "" : $scope.session.data.curves_table[i].Max,
                                Threshold:  $scope.session.data.curves_table[i].Threshold
                            });
                        }
                    }
                    //Unlock tabs
                    $('#tab1 button').prop('disabled', false);
                    $('#tab2 button').prop('disabled', false);

                    hnd.loadData(data);
                    updateProfit();
                    exportTableCsv($scope.session.data.config_name, headers, hnd.getData(), $scope.session.user_details.username, "linkExportMainData");
                    exportSessionCsv($scope.session.data, hnd.getData(), "linkExportMainData2");

                    getCurves(function(){
                        switch($scope.sel_tab) {
                            case 0://MAIN
                                $scope.draw_curves();
                                $scope.draw_allocation();
                                break;
                            case 1://Profit Curves
                                $scope.draw_profit_curve();
                                break;
                            case 2://Budget
                                $scope.draw_area();
                                break;
                        }
                        $timeout(function(){$scope.showLoading=false;}, 100);
                    });
                }
            });
        }

    }

    var resizeStable2 = function(){
        var height_thead = '28';
        if(typeof $('#' + divTable2 + ' thead').css('height') != 'undefined'){
            height_thead = $('#' + divTable2 + ' thead').css('height').slice(0,-2);
        }
        $('#' + divHeader2).css('display', 'block');
        $('#' + divMain2).css({
            'overflow': 'scroll',
            'position': 'relative',
            'top': '-' + height_thead + 'px',
        });
        MakeStaticHeader(divTable2, 320, 540, height_thead, false, divHeader2, divMain2, divFooter2);
    }


    /**
     * INIT
     */
    SessionSvc.currentSlide = "main";
    $scope.session = SessionSvc.currentSession;
    $scope.unitsSel = $scope.session.data.unit_sel;
    $scope.extra_units = $("<div />").html(["$", "&#163;", "&euro;"][langss.indexOf(langNumeral)]).text() + getUSel();
    get_sizes();
    $scope.update_budget_format($scope.session.data.budget);
    default_settings();
    $scope.create_hnd();
    $scope.optimise();

    window.onresize = function(event) {
        get_sizes();
        var colWidths = [];
        for(var i=0; i<headers.length; i++){
            colWidths.push(w_table/headers.length);
        }
        if(loaded){

        }
        hnd.updateSettings({colWidths: colWidths});
    }
    loaded = true;
});