/**
 * Created by marc.rivelles on 27/01/14.
 */
'use strict';
var broApp = angular.module('broApp', [
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngRoute',
        'ngAnimate',
        'ui.bootstrap',
        'mgcrea.ngStrap',
        'angularFileUpload'
    ]).config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'private/views/welcome.html',
                controller: 'WelcomeCtrl'
            })
            .when('/main', {
                templateUrl: 'private/views/main.html',
                controller: 'MainCtrl'
            })
            .otherwise({redirectTo: '/'});
        $locationProvider.html5Mode(true);
    });