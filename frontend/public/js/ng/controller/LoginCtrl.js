/**
 * Created by Marc.Rivelles on 13/11/2015.
 */
var app = angular.module('login-app', []);
app.controller('LoginCtrl', function($scope, $http) {
    $scope.app_name = "BOP";
    $scope.user = '';
    $scope.pass = '';
    $scope.error_text = "";

    $scope.login = function () {
        var config = {
            method: 'POST',
            url: '/login',
            data: {
                username: $scope.user,
                password: $scope.pass
            }
        }
        $http(config)
            .success(function (data, status, headers, config) {
                window.location.href = '/private/';
            })
            .error(function (data, status, headers, config) {
                $scope.error_text = data.Message;
                error_message_show();
            });
    };
});

/**
 * Display Error Message
 */
var error_message_show = function(){
    document.getElementsByClassName("error_div")[0].style.display = "block";
    setTimeout(function(){
        document.getElementsByClassName("error_div")[0].style.display = "none";
    }, 1500);
}