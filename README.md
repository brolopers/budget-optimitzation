# README #

BPO is the acronym of Budget Optimization Package. 

The idea of this tool is upload a curve data points with a model and optimize over a budget.
### Process and How to use that tool ###
We start from we have some data points that we want to know the curve and optimize these ones, 
to get the optimal amount distributed across the elements.

* First we create the file to upload.
* Upload the file.
* Put the amount that we want to distribute.
* If we want to modify the amount we can use min, max, threshold.
  -Min: Minimum to spend, greater than 0, if is equal to zero, there is no spend.
  -Max: Max to spend or empty it will be max unlimited. 
  -Threshold: The amount that has to be exceed to allow allocate amount in this element.
* Press optimize as many times you want to view how efficient is distributed using the constraints.
* Export the result
* Export the session to import and continue other day (Can be using DB but for demo it's ok)

### Data Uploaded ###

Consists in the next Row Elements:
* Name of the curve / element.
Method:
* 0 for Logistic Curve.
* 1 for Exponential Curve.
* 2 for Negative Exponential Curve.

* Current Spend (Only for comparison).
* Number of points.
* X points (Example Spend)
* Y Points (Example Sales)

A screen sample:

### Login Screen ###
![login.png](https://bitbucket.org/repo/6GbXEx/images/2725328953-login.png)

### Welcome Screen ###
![load.png](https://bitbucket.org/repo/6GbXEx/images/1697742303-load.png)

### First Main Screen ###
![sc1.png](https://bitbucket.org/repo/6GbXEx/images/299084775-sc1.png)

### Second Main Screen ###
![sc3.png](https://bitbucket.org/repo/6GbXEx/images/455380421-sc3.png)

### Third Main Screen ###
![sc4.png](https://bitbucket.org/repo/6GbXEx/images/1007054535-sc4.png)

### Options Main Screen ###
![sc2.png](https://bitbucket.org/repo/6GbXEx/images/1560639791-sc2.png)