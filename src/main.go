package main

import (
	"net/http"
	"encoding/csv"
	"encoding/json"
	"strconv"
	"strings"
	"time"
	"log"
	"errors"

	fit "bop/fitcurves"
	opt "bop/optimisecurves"
	db  "bop/db"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"labix.org/v2/mgo"
	"math"
)

// 1MB - Upload Files
const MAX_MEMORY = 1 * 1024 * 1024
const SERVER_TYPE = "Test" //"Production"
const PORT		 = ":8080"
const PORT_SSL	 = ":443"

const server_db	 = "localhost"
const port  	 = "27017"

const ssl_cert 	 = "./ssl/ssl_cert.crt"
const ssl_key 	 = "./ssl/decrypted_key.key"

var session *mgo.Session

/*Curves*/
type Opt_channel_up struct {
	Name 		string
	Function 	int
	RawX        []float64
	RawY        []float64
	Raw_spend	float64
}

/*Session*/
type Opt_channel_up2 struct {
	Name 		string
	Function 	int
	RawX        []float64
	RawY        []float64
	Raw_spend	float64
	Spend		float64
	Min 		float64
	Max 		float64
	Threshold   float64
	Visible		int
}

type Calculate  struct {
	Functions		[]int
	Scales			[]float64
	FittedPars		[][]float64
	Values 			[]float64
}

// Parse Array of String to Float64, if fail the conversion add 0.0
func StrconvArr(array []string) []float64{
	numbers := make([]float64, len(array))
	for i, arg := range array {
		numbers[i] = 0.0
		if n, err := strconv.ParseFloat(arg, 64); err == nil {
			numbers[i] = n
		}
	}
	return numbers
}

var store = sessions.NewCookieStore([]byte("abracadabra"))

func getSession() *mgo.Session {
	s, err := mgo.Dial("mongodb://" + server_db + ":" + port)
	if err != nil {
		panic(err)
	}
	return s
}

func Login_http(w http.ResponseWriter, r *http.Request){
	s := session.Clone()
	defer s.Close()

	usc := db.NewUserController(session)
	var user_login db.User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user_login)
	//log.Println(user_login)
	if err == nil {
		us_val, err := usc.Validate_user_pass(user_login.Username, user_login.Password)
		if err == nil {
			session_cok, err := store.Get(r, "session-name")
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}

			session_cok.Values["isAuthenticated"] = "true"
			session_cok.Values["username"] = user_login.Username
			session_cok.Values["id"] = us_val.Id.Hex()
			session_cok.Values["lang"] = us_val.Currency
			session_cok.Values["lang_web"] = strings.Split(r.Header["Accept-Language"][0], ",")[0]
			current_time := time.Now()
			session_cok.Values["exp_date"] = current_time.Add(time.Minute * 60).Format(time.RFC3339)
			session_cok.Save(r, w)
		}else{
			js, err := json.Marshal(struct{ Message string }{err.Error()})
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.WriteHeader(500)
			w.Write(js)
		}
	}else{
		js, err := json.Marshal(struct{ Message string }{err.Error()})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(500)
		w.Write(js)
	}
}

func Login_Token(w http.ResponseWriter, r *http.Request){
	s := session.Clone()
	defer s.Close()

	usc := db.NewUserController(session)
	var user_token struct{
		Username	string	`json:'username'`
		Token		string	`json:'token'`
	}
	var err error
	if len(r.URL.Query()["username"]) == 1{
		user_token.Username = r.URL.Query()["username"][0]
	}else{
		err = errors.New("username not provided")
	}
	if len(r.URL.Query()["token"]) == 1{
		user_token.Token = r.URL.Query()["token"][0]
	}else{
		err = errors.New("token not provided")
	}
	//log.Println(user_token)

	if err == nil {
		us_val, err := usc.Validate_user_token(user_token.Username, user_token.Token)
		if err == nil {
			session_cok, err := store.Get(r, "session-name")
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}

			session_cok.Values["isAuthenticated"] = "true"
			session_cok.Values["username"] = user_token.Username
			session_cok.Values["id"] = us_val.Id.Hex()
			session_cok.Values["lang"] = us_val.Currency
			session_cok.Values["lang_web"] = strings.Split(r.Header["Accept-Language"][0], ",")[0]
			current_time := time.Now()
			session_cok.Values["exp_date"] = current_time.Add(time.Minute * 60).Format(time.RFC3339)
			session_cok.Save(r, w)
			http.Redirect(w, r, "/private/", 302)
		}else{
			js, err := json.Marshal(struct{ Message string }{err.Error()})
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.WriteHeader(500)
			w.Write(js)
		}
	}else{
		js, err := json.Marshal(struct{ Message string }{err.Error()})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(500)
		w.Write(js)
	}
}

func Logout_http(w http.ResponseWriter, r *http.Request){
	session_cok, err := store.Get(r, "session-name")
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	session_cok.Values["isAuthenticated"] = "false"
	session_cok.Save(r, w)
	http.Redirect(w, r, "/", 302)
}

func CurrentUser_http(w http.ResponseWriter, r *http.Request){
	session_cok, err := store.Get(r, "session-name")
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	username, _ := session_cok.Values["username"].(string)
	id, _ := session_cok.Values["id"].(string)
	lang, _ := session_cok.Values["lang"].(string)
	lang_web, _ := session_cok.Values["lang_web"].(string)

	data_to_json := struct{
		Username 	string	`json:"username"`
		Id			string	`json:"id"`
		Lang		string	`json:"lang"`
		Lang_web	string	`json:"lang_web"`
	}{
		username,
		id,
		lang,
		lang_web,
	}
	js, err := json.Marshal(data_to_json)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func generateCurvesSample(c_in []fit.Curvefit_input) ([]fit.Curvefit_table, float64, float64) {
	budget := 0.0
	total := 0.0
	sample := make([]fit.Curvefit_table, len(c_in))
	for x := 0; x < len(c_in); x++ {
		sample[x] = fit.Curvefit_table {
			Name: 			c_in[x].Name,
			Color:			"",
			Function: 	    c_in[x].Function,
			Visible:		1,
			Spend:			0.0,
			Sales:			0.0,
			Min:			0.0,
			Max:			-1.0,
			Threshold:		0.0,
		}
		budget += c_in[x].Raw_spend
	}
	total = budget * 2
	return sample, budget, total
}

func FitCurves(input []fit.Curvefit_input) []fit.Curvefit_output {
	output := make([]fit.Curvefit_output, len(input))
	cf := new(fit.CurveFit)
	for row, _ := range input {
		cf.Input.Function = input[row].Function
		cf.Input.RawX = input[row].RawX
		cf.Input.RawY = input[row].RawY
		cf.Initialise()
		cf.Fit()

		//Output
		output[row].Name = input[row].Name
		output[row].FittedPars = cf.Output.FittedPars
		output[row].FittedPars = append(output[row].FittedPars, cf.Output.ScaleX)
		output[row].FittedPars = append(output[row].FittedPars, cf.Output.ScaleY)
		output[row].ScaledX = cf.Output.ScaledX
		output[row].ScaledY = cf.Output.ScaledY
		output[row].ScaleX = cf.Output.ScaleX
		output[row].ScaleY = cf.Output.ScaleY
		output[row].Residual = cf.Residual(cf.Output.FittedPars)
	}
	return output
}

func UploadOrcaFile(w http.ResponseWriter, r *http.Request){
	if err := r.ParseMultipartForm(MAX_MEMORY); err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
	}

	if RequireAuthFunc(w, r) {
		for _, fileHeaders := range r.MultipartForm.File {
			var chann_curves []Opt_channel_up
			for _, fileHeader := range fileHeaders {
				file, err := fileHeader.Open()
				if err != nil {
					break
				}
				defer file.Close()

				csvr := csv.NewReader(file)
				nrows := 0
				for {
					row, err := csvr.Read()
					if err != nil {
						break
					}

					//Jump The headers
					if nrows > 0 {
						// Exit when is empty
						//log.Println(row[0])
						if row[0] == "" {
							break
						} else {
							n_points := 0
							if n, err := strconv.Atoi(row[3]); err == nil {
								n_points = n
							}
							c_spend := 0.0
							if n, err := strconv.ParseFloat(row[2], 64); err == nil {
								c_spend = n
							}
							n_func := 0
							if n, err := strconv.Atoi(row[1]); err == nil {
								n_func = n
							}
							channel_row := Opt_channel_up{
								Function:    n_func,
								Name:        row[0],
								Raw_spend:   math.Abs(c_spend),
								RawX:        StrconvArr(row[4: 4 + n_points]),
								RawY:        StrconvArr(row[4 + n_points: 4 + (n_points * 2)]),
							}
							chann_curves = append(chann_curves, channel_row)
						}
					}
					nrows++
				}
				break    //Only one file allowed
			}
			//fit curves
			//Save ??
			chann_curves_in := make([]fit.Curvefit_input, len(chann_curves))
			for x := 0; x < len(chann_curves); x++ {
				chann_curves_in[x].Function = chann_curves[x].Function
				chann_curves_in[x].Name = chann_curves[x].Name
				chann_curves_in[x].RawX = chann_curves[x].RawX
				chann_curves_in[x].RawY = chann_curves[x].RawY
				chann_curves_in[x].Raw_spend = chann_curves[x].Raw_spend

			}
			var new_session db.Session_struct

			new_session.Curves_in = chann_curves_in
			new_session.Config_name = r.FormValue("confName")
			new_session.Username = r.FormValue("username")
			new_session.Curves_table, new_session.Budget, new_session.Max_spend = generateCurvesSample(chann_curves_in)
			new_session.Units = 1
			new_session.Num_points = 30
			new_session.Num_incr = 30
			new_session.Middle_pp = 1
			new_session.Unit_sel = "Auto Format"
			new_session.Curves_out = FitCurves(chann_curves_in)

			js, err := json.Marshal(new_session)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			w.Header().Set("Content-Type", "application/json")
			w.Write(js)
		}
	}else{
		http.Redirect(w, r, "/logout", 302)
	}
}

func UploadOrcaSession(w http.ResponseWriter, r *http.Request){
	if err := r.ParseMultipartForm(MAX_MEMORY); err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
	}
	if RequireAuthFunc(w, r) {
		for _, fileHeaders := range r.MultipartForm.File {
			var chann_curves []Opt_channel_up2
			for _, fileHeader := range fileHeaders {
				file, err := fileHeader.Open()
				if err != nil {
					break
				}
				defer file.Close()

				csvr := csv.NewReader(file)
				nrows := 0
				for {
					row, _ := csvr.Read()
					if len(row) == 0 {
						break
					}

					//Jump The headers
					if nrows > 0 {
						// Exit when is empty
						if row[0] == "" {
							break
						} else {
							n_points := 0
							if n, err := strconv.Atoi(row[8]); err == nil {
								n_points = n
							}
							c_spend := 0.0
							if n, err := strconv.ParseFloat(row[2], 64); err == nil {
								c_spend = n
							}
							o_spend := 0.0
							if n, err := strconv.ParseFloat(row[3], 64); err == nil {
								o_spend = n
							}
							o_min := 0.0
							if n, err := strconv.ParseFloat(row[4], 64); err == nil {
								o_min = n
							}
							o_max := 0.0
							if row[5] == "" || row[5] == " " {
								o_max = -1.0
							}else{
								if n, err := strconv.ParseFloat(row[5], 64); err == nil {
									o_max = n
								}
							}
							o_thr := 0.0
							if n, err := strconv.ParseFloat(row[6], 64); err == nil {
								o_thr = n
							}
							n_func := 0
							if n, err := strconv.Atoi(row[1]); err == nil {
								n_func = n
							}
							visible := 1
							if n, err := strconv.Atoi(row[7]); err == nil {
								visible = n
							}
							channel_row := Opt_channel_up2{
								Function:    n_func,
								Name:        row[0],
								Raw_spend:   math.Abs(c_spend),
								RawX:        StrconvArr(row[9: 9 + n_points]),
								RawY:        StrconvArr(row[9 + n_points: 9 + (n_points * 2)]),
								Spend:		 math.Abs(o_spend),
								Min: 		 math.Abs(o_min),
								Max: 		 o_max,
								Threshold:   o_thr,
								Visible:	 visible,
							}
							chann_curves = append(chann_curves, channel_row)
						}
					}
					nrows++
				}
				break    //Only one file allowed
			}
			//fit curves
			//Save ??
			chann_curves_in := make([]fit.Curvefit_input, len(chann_curves))
			for x := 0; x < len(chann_curves); x++ {
				chann_curves_in[x].Function = chann_curves[x].Function
				chann_curves_in[x].Name = chann_curves[x].Name
				chann_curves_in[x].RawX = chann_curves[x].RawX
				chann_curves_in[x].RawY = chann_curves[x].RawY
				chann_curves_in[x].Raw_spend = chann_curves[x].Raw_spend
			}
			var new_session db.Session_struct

			new_session.Curves_in = chann_curves_in
			new_session.Config_name = r.FormValue("confName")
			new_session.Username = r.FormValue("username")
			new_session.Curves_table, new_session.Budget, new_session.Max_spend = generateCurvesSample(chann_curves_in)
			budget := 0.0
			for x := 0; x < len(chann_curves); x++ {
				if chann_curves[x].Visible == 1{
					budget += chann_curves[x].Spend
				}
				new_session.Curves_table[x].Spend = chann_curves[x].Spend
				new_session.Curves_table[x].Min = chann_curves[x].Min
				new_session.Curves_table[x].Max = chann_curves[x].Max
				new_session.Curves_table[x].Threshold = chann_curves[x].Threshold
				new_session.Curves_table[x].Visible = chann_curves[x].Visible
			}
			new_session.Budget = budget
			new_session.Units = 1
			new_session.Num_points = 30
			new_session.Num_incr = 30
			new_session.Middle_pp = 1
			new_session.Unit_sel = "Auto Format"
			new_session.Curves_out = FitCurves(chann_curves_in)

			js, err := json.Marshal(new_session)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			w.Header().Set("Content-Type", "application/json")
			w.Write(js)
		}
	}else{
		http.Redirect(w, r, "/logout", 302)
	}
}

func SetUserLang(w http.ResponseWriter, r *http.Request){
	var user_details struct{
		Id		string
		Lang	string
	}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user_details); err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	if RequireAuthFunc(w, r) {
		usc := db.NewUserController(session)
		_,err := usc.Update_currency(user_details.Id, user_details.Lang)
		if  err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte{})
	}else{
		http.Redirect(w, r, "/logout", 302)
	}
}

type output_opt struct {
	Spend 			[]float64 	`json:"spend"`
	Sales 			[]float64 	`json:"sales"`
	Msg				string		`json:"msg"`
}

func Optimiser_http(w http.ResponseWriter, r *http.Request){
	oc := new(opt.OptimiseCurves)
	var optim struct {
		Input			[]opt.OptimiseChannel
		Overall			float64
	}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&optim); err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if RequireAuthFunc(w, r) {
		var output output_opt
		oc.Input.Channels = optim.Input
		oc.Overall = optim.Overall
		res, msg := oc.Calculate()
		output.Spend = res
		output.Msg = msg
		if strings.ToLower(msg) == "ok" {
			output.Sales = oc.SalesCurve(res)
		}
		js, err := json.Marshal(output)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}else{
		http.Redirect(w, r, "/logout", 302)
	}
}

type output_curv struct {
	Sales			[][]float64
	Spend			[][]float64
	Profit_sales	[][]float64
	Profit_spend	[][]float64
	PPoint_sales	[]float64
	PPoint_spend	[]float64
	Area_data		[]opt.BudgetOutput1
}

func Curves_http(w http.ResponseWriter, r *http.Request){
	oc := new(opt.OptimiseCurves)
	ftc := new(fit.CurveFit)
	var optim struct {
		Input       []opt.OptimiseChannel
		OutputFit   []fit.Fit_output
		Fncs        []int
		Total_spend []float64
		Max_spend   float64
		Min_spend	float64
		Num_incr	int
		Num_points	int
	}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&optim); err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if RequireAuthFunc(w, r) {
		var output output_curv
		oc.Input.Channels = optim.Input
		output.Spend, output.Sales = ftc.CurvesChart(optim.Num_points, optim.Total_spend, optim.OutputFit, optim.Fncs)
		output.Profit_spend, output.Profit_sales, output.PPoint_sales, output.PPoint_spend = oc.ProfitChart(optim.Num_incr, optim.Max_spend, optim.Min_spend)
		output.Area_data = oc.BudgetChart(output.Profit_spend, output.Profit_sales)

		js, err := json.Marshal(output)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}else{
		http.Redirect(w, r, "/logout", 302)
	}
}

func RequireAuth(h http.Handler) http.Handler {

	return http.HandlerFunc(func (w http.ResponseWriter, r *http.Request){
		session_cok, err := store.Get(r, "session-name")
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		cook_date, _ := session_cok.Values["exp_date"].(string)
		time_parsed, err := time.Parse(time.RFC3339, cook_date)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		if time_parsed.Sub(time.Now()) > time.Second * 50 {
			h.ServeHTTP(w, r)
		}else{
			http.Redirect(w, r, "/logout", 302)
		}
	})
}

func RequireAuthFunc(w http.ResponseWriter, r *http.Request) bool{
	authorised := true
	if SERVER_TYPE == "Production" {
		authorised = false
		session_cok, err := store.Get(r, "session-name")
		if err != nil {
			http.Error(w, err.Error(), 500)
			return false
		}
		cook_date, _ := session_cok.Values["exp_date"].(string)
		time_parsed, err := time.Parse(time.RFC3339, cook_date)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return false
		}

		if time_parsed.Sub(time.Now()) > time.Second * 50 {
			authorised = true
		}
	}
	return authorised
}

func init() {
	if SERVER_TYPE == "Production" {
		session = getSession()
	}
}

func redir(w http.ResponseWriter, req *http.Request) {
	http.Redirect(w, req, "https://nametool.com"+req.RequestURI, http.StatusMovedPermanently)
}

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/login", Login_http).Methods("POST")
	router.HandleFunc("/loginToken/", Login_Token).Methods("GET")
	router.HandleFunc("/logout", Logout_http)
	router.HandleFunc("/currentUser", CurrentUser_http).Methods("GET")
	router.HandleFunc("/upDataConfig", UploadOrcaFile).Methods("POST")
	router.HandleFunc("/upDataConfig2", UploadOrcaSession).Methods("POST")

	router.HandleFunc("/setLang", SetUserLang).Methods("POST")
	router.HandleFunc("/optimise", Optimiser_http).Methods("POST")
	router.HandleFunc("/curves", Curves_http).Methods("POST")
	if SERVER_TYPE == "Production" {
		router.PathPrefix("/private/").Handler(RequireAuth(http.FileServer(http.Dir("./frontend/")))).Methods("GET")
		router.PathPrefix("/").Handler(http.FileServer(http.Dir("./frontend/public/"))).Methods("GET")
	}else{
		//Compile only
		//router.PathPrefix("/private/").Handler(RequireAuth(http.FileServer(http.Dir("frontend/")))).Methods("GET")
		//router.PathPrefix("/").Handler(http.FileServer(http.Dir("frontend/public/"))).Methods("GET")
		router.PathPrefix("/").Handler(http.FileServer(http.Dir("./frontend/"))).Methods("GET")
	}
	http.Handle("/", router)

	log.Println("Server Running at port ", PORT, PORT_SSL)

	if SERVER_TYPE == "Production" {
		go http.ListenAndServeTLS(PORT_SSL, ssl_cert, ssl_key , nil)
		log.Fatal(http.ListenAndServe(PORT, http.HandlerFunc(redir)))
	}else{
		log.Fatal(http.ListenAndServe(PORT,nil))
	}
}
