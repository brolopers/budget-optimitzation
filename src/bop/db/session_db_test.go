package db

import (
	"labix.org/v2/mgo"
	"testing"
)


func TestSession_db(t *testing.T) {
	s, err := mgo.Dial("mongodb://" + server_test + ":" + port_test)
	if err != nil {
		panic(err)
	}
	ses := NewSessionController(s)

	//Dummy Session
	var new_session Session_struct
	new_session.Username = "paco_lumbreras"
	ids := make([]string, 2)


	//Insert Session
	new_session, err = ses.New(new_session)
	if err != nil {
		t.Error(" Insert User FAIL: ", err)
	}
	ids[0] = new_session.Id.Hex()

	//Save Session
	new_session.Username = "new_user"
	new_session, err = ses.Save(new_session.Id.Hex(), new_session)
	if err != nil {
		t.Error(" Save Session FAIL: ", err)
	}
	ids[1] = new_session.Id.Hex()

	//Save As Session
	new_session.Username = "new_user2"
	last_session, err := ses.Save_as(new_session)
	if err != nil {
		t.Error(" Save Session As FAIL: ", err)
	}

	//Get One Session
	last_session, err = ses.Get_one(last_session.Id.Hex())
	if err != nil {
		t.Error(" Get One Session As FAIL: ", err)
	}

	//Get All Sessions
	_, err = ses.Get_all(last_session.Username)
	if err != nil {
		t.Error(" Get All Sessions As FAIL: ", err)
	}

	//Delete Session
	err = ses.Delete(last_session.Id.Hex())
	if err != nil {
		t.Error(" Delete Session FAIL: ", err)
	}

	//Delete All IDs Sessions
	err = ses.Delete_all(ids)
	if err != nil {
		t.Error(" Delete All Sessions FAIL: ", err)
	}
}

