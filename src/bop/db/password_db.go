package db

import (
	"golang.org/x/crypto/bcrypt"
	"encoding/hex"
	"crypto/md5"
	"math/rand"
)

const (
	DefaultCost int = 4 // the cost that will actually be set if a cost below MinCost is passed into GenerateFromPassword
	SaltLength	int = 9	// Old function cost
)

func CreateHash(secret string) string {
	hash, _ := bcrypt.GenerateFromPassword([]byte(secret), DefaultCost)
	return hex.EncodeToString(hash)
}

func ValidateHash(password string, hash string) bool {
	hash_dec, _ := hex.DecodeString(hash)
	resp := bcrypt.CompareHashAndPassword([]byte(hash_dec), []byte(password))
	return resp == nil
}



/* Old Library */
func CreateHashOld(password string) string {
	salt := generateSalt()
	hash := md5_port(password + salt)
	return salt + hash
}

func ValidateHashOld(password string, hash string) bool {
	salt := hash[0:SaltLength]
	valid_hash := salt + md5_port(password + salt)
	return hash == valid_hash
}

/* PORT MD5 Nodejs */
func md5_port(secret string) string {
	h := md5.New()
	h.Write([]byte(secret))
	return hex.EncodeToString(h.Sum(nil))
}

func generateSalt() string {
	set_salt := []string{"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f","g","h",
						 "i","j","k","l","m","n","o","p","q","u","r","s","t","u","v","w","x","y",
						 "z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q",
						 "U","R","S","T","U","V","W","X","Y","Z"}
	set_len := len(set_salt)
	salt := ""

	for x := 0; x < SaltLength; x++ {
		salt += set_salt[rand.Intn(set_len)]
	}
	return salt
}