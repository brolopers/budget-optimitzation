package db

import (
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
	"time"
	"errors"
)


type UserController struct {
	session *mgo.Session
}

type User struct {
	Id 				bson.ObjectId	`json:"id"        bson:"_id,omitempty"`
	Username		string			`json:"username"`
	Password		string			`json:"password"`
	Number_loggins	int				`json:"number_loggins"`
	Creation_date	string			`json:"creation_date"`
	Attemps			int				`json:"attemps"`
	Currency		string			`json:"currency"`
}

func NewUserController(s *mgo.Session) *UserController {
	return &UserController{s}
}

func (uc UserController) Create_user(username string, password string) (User, error){
	//Check User First
	u := User{}
	query := bson.M{"username": username}
	err := uc.session.DB(database).C(c_users).Find(query).One(&u)
	if err != nil{
		new_user := User{
			Id:              bson.NewObjectId(),
			Username:        username,
			Password:        CreateHash(password),
			Creation_date:   time.Now().String(),
			Number_loggins:  0,
			Attemps:         0,
			Currency:        "en",
		}
		err = uc.session.DB(database).C(c_users).Insert(new_user)
	}else{
		err = errors.New("Existing username")
	}
	return u, err
}

func (uc UserController) Validate_user_pass(username string, password string) (User, error){
	query := bson.M{"username": username}
	var error_throw error
	u := User{}
	err := uc.session.DB(database).C(c_users).Find(query).One(&u)
	if err == nil {
		if ValidateHashOld(password, u.Password) {
			if u.Attemps >= max_attemps {
				error_throw = errors.New("User locked, contact with IT.")
			}else{
				u.Attemps = 0
			}
		}else {
			if u.Attemps >= max_attemps {
				error_throw = errors.New("User locked, contact with IT.")
			}else{
				error_throw = errors.New("User / Password do not match.")
				u.Attemps += 1
			}
		}
		u.Number_loggins += 1
		query_upd := bson.M{"$set": bson.M{"attemps": u.Attemps, "number_loggins": u.Number_loggins}}
		_ = uc.session.DB(database).C(c_users).UpdateId( u.Id, query_upd)
	}else{
		error_throw = errors.New("User / Password do not match.")
	}
	return u, error_throw
}

func (uc UserController) Validate_user_token(username string, token string) (User, error){
	query := bson.M{"username": username}
	var error_throw error
	u := User{}
	err := uc.session.DB(database).C(c_users).Find(query).One(&u)
	if err == nil {
		if token == u.Password {
			if u.Attemps >= max_attemps {
				error_throw = errors.New("User locked, contact with IT.")
			}else{
				u.Attemps = 0
			}
		}else {
			if u.Attemps >= max_attemps {
				error_throw = errors.New("User locked, contact with IT.")
			}else{
				error_throw = errors.New("User / Password do not match.")
				u.Attemps += 1
			}
		}
		u.Number_loggins += 1
		query_upd := bson.M{"$set": bson.M{"attemps": u.Attemps, "number_loggins": u.Number_loggins}}
		_ = uc.session.DB(database).C(c_users).UpdateId( u.Id, query_upd)
	}else{
		error_throw = errors.New("User / Password do not match.")
	}
	return u, error_throw
}

func (uc UserController) Update_password(username_id string, password string) (User, error){
	query := bson.ObjectIdHex(username_id)

	u := User{}
	err := uc.session.DB(database).C(c_users).FindId(query).One(&u)
	if err == nil {
		query_upd := bson.M{"$set": bson.M{"password": CreateHashOld(password)}}
		err = uc.session.DB(database).C(c_users).UpdateId( query, query_upd)
	}
	return u, err
}

func (uc UserController) Update_currency(username_id string, currency string) (User, error){
	query := bson.ObjectIdHex(username_id)

	u := User{}
	err := uc.session.DB(database).C(c_users).FindId(query).One(&u)
	if err == nil {
		query_upd := bson.M{"$set": bson.M{"currency": currency}}
		err = uc.session.DB(database).C(c_users).UpdateId( query, query_upd)
	}
	return u, err
}

func (uc UserController) Get_currency(username_id string) (User, error){
	query := bson.ObjectIdHex(username_id)

	u := User{}
	err := uc.session.DB(database).C(c_users).FindId(query).One(&u)
	return u, err
}


func (uc UserController) Delete_user(username_id string) error{
	query := bson.ObjectIdHex(username_id)

	err := uc.session.DB(database).C(c_users).RemoveId(query)
	return err
}