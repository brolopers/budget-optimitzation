package db

import (
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
	"time"
	fit "bop/fitcurves"
)


type SessionController struct {
	session *mgo.Session
}

type Session_struct struct {
	Id 				bson.ObjectId			`json:"id"        bson:"_id,omitempty"`
	Username		string					`json:"username"`
	Config_name		string					`json:"config_name"`
	Curves			int						`json:"curves"`
	Curves_in		[]fit.Curvefit_input	`json:"curves_in"`
	Curves_out		[]fit.Curvefit_output	`json:"curves_out"`
	Curves_table	[]fit.Curvefit_table	`json:"curves_table"`
	Budget			float64					`json:"budget"`
	Last_saved_date	string					`json:"last_saved_date"`
	Creation_date	string					`json:"creation_date"`
	Units			int						`json:"units"`
	Num_points		int						`json:"num_points"`
	Num_incr		int						`json:"num_incr"`
	Max_spend		float64					`json:"max_spend"`
	Middle_pp		int						`json:"middle_pp"`
	Unit_sel		string					`json:"unit_sel"`
}

type Session_save struct {
	Id 				string					`json:"id"`
	Username		string					`json:"username"`
	Config_name		string					`json:"config_name"`
	Curves			int						`json:"curves"`
	Curves_in		[]fit.Curvefit_input	`json:"curves_in"`
	Curves_out		[]fit.Curvefit_output	`json:"curves_out"`
	Curves_table	[]fit.Curvefit_table	`json:"curves_table"`
	Budget			float64					`json:"budget"`
	Last_saved_date	string					`json:"last_saved_date"`
	Creation_date	string					`json:"creation_date"`
	Units			int						`json:"units"`
	Num_points		int						`json:"num_points"`
	Num_incr		int						`json:"num_incr"`
	Max_spend		float64					`json:"max_spend"`
	Middle_pp		int						`json:"middle_pp"`
	Unit_sel		string					`json:"unit_sel"`
}

type Session_min_struct struct {
	Id 				bson.ObjectId			`json:"id"        bson:"_id,omitempty"`
	Config_name		string					`json:"config_name"`
	Curves			int						`json:"curves"`
	Last_saved_date	string					`json:"last_saved_date"`
	Creation_date	string					`json:"creation_date"`
}

func NewSessionController(s *mgo.Session) *SessionController {
	return &SessionController{s}
}

func (uc SessionController) New(new_session Session_struct) (Session_struct, error){
	//Update missing things
	new_session.Id = bson.NewObjectId()
	new_session.Curves = len(new_session.Curves_in)
	new_session.Creation_date = time.Now().String()
	new_session.Last_saved_date = time.Now().String()
	err := uc.session.DB(database).C(c_sessions).Insert(new_session)
	return new_session, err
}

func (uc SessionController) Save(ses_id string, c_session Session_save) (Session_save, error){
	query := bson.M{"_id":  bson.ObjectIdHex(ses_id)}
	c_session.Last_saved_date = time.Now().String()
	err := uc.session.DB(database).C(c_sessions).Update( query, c_session)
	return c_session, err
}

func (uc SessionController) Save_as(new_session Session_struct) (Session_struct, error){
	new_session.Id = bson.NewObjectId()
	new_session.Creation_date = time.Now().String()
	new_session.Last_saved_date = time.Now().String()
	err := uc.session.DB(database).C(c_sessions).Insert(new_session)
	return new_session, err
}

func (uc SessionController) Delete(ses_id string) error{
	query := bson.ObjectIdHex(ses_id)
	err := uc.session.DB(database).C(c_sessions).RemoveId(query)
	return err
}

func (uc SessionController) Delete_all(ses_id []string) error{
	new_ids := make([]bson.ObjectId, len(ses_id))
	for i := 0; i < len(ses_id); i++ {
		new_ids[i] = bson.ObjectIdHex(ses_id[i])
	}
	query := bson.M{"_id": bson.M{"$in": new_ids}}
	_,err := uc.session.DB(database).C(c_sessions).RemoveAll(query)
	return err
}

func (uc SessionController) Get_one(ses_id string) (Session_struct, error){
	var s Session_struct
	query := bson.M{"_id": bson.ObjectIdHex(ses_id)}
	err := uc.session.DB(database).C(c_sessions).Find(query).One(&s)
	return s, err
}

func (uc SessionController) Get_all(username string) ([]Session_min_struct, error){
	var ses []Session_min_struct
	query := bson.M{"username": username}
	s_param := "-last_saved_date"
	err := uc.session.DB(database).C(c_sessions).Find(query).Sort(s_param).All(&ses)
	return ses, err
}
