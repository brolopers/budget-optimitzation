package db

import (
	"testing"
)

func TestPassword(t *testing.T) {
	my_pass := "lacasaverde12456!"
	hash_pass := CreateHash(my_pass)

	if  ValidateHash("asasa", hash_pass) != false || ValidateHash(my_pass, hash_pass) != true{
		t.Error(" FAIL Password new hash", my_pass, hash_pass)
	}


	if  ValidateHashOld(my_pass, "RXCZUSmuab14d656fd4e97660879f494f7d32d69d") != true{
		t.Error(" FAIL Password old hash", my_pass, hash_pass)
	}

	if  ValidateHashOld("f3rd999", "YTfPDUUZ678fe297c9cc984fbe581bb5eaeef11d5") != true{
		t.Error(" FAIL Password old hash", my_pass, hash_pass)
	}

}