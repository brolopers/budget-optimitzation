package db

const database  	= "profitpac"
const c_users  		= "users"
const c_sessions  	= "sessions"
const max_attemps  	= 5

/**
	Test CONFIG
 */
const server_test  	= "localhost"
const port_test  	= "5895"