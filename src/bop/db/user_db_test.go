package db

import (
	"labix.org/v2/mgo"
	"testing"
)

func TestUser_db(t *testing.T) {
	s, err := mgo.Dial("mongodb://" + server_test + ":" + port_test)
	if err != nil {
		panic(err)
	}
	usc := NewUserController(s)

	//CREATE USER
	_, err = usc.Create_user("paco_lumbreras", "elmejordetodos")
	if err != nil {
		t.Error(" Create User FAIL: ", err)
	}

	//User Duplicate Check
	_, err = usc.Create_user("paco_lumbreras", "elmejordetodos")
	if err == nil {
		t.Error(" Create User (Duplicate Validation) FAIL: ")
	}

	//VALIDATE USER & PASSWORD
	_, err = usc.Validate_user_pass("paco_lumbreras", "elmejordetodos")
	if err != nil {
		t.Error(" Validate User FAIL: ", err)
	}

	//Wrong Password Check
	user, err := usc.Validate_user_pass("paco_lumbreras", "otherpass")
	if err == nil {
		t.Error(" Validate User (Wrong Pass Check) FAIL: ", err)
	}

	//Update Password
	_, err = usc.Update_password(user.Id.Hex(), "newpass")
	if err != nil {
		t.Error(" Update password FAIL: ", err)
	}

	//Update Lang
	_, err = usc.Update_currency(user.Id.Hex(), "en-gb")
	if err != nil {
		t.Error(" Update currency FAIL: ", err)
	}

	//Delete User
	err = usc.Delete_user(user.Id.Hex())
	if err != nil {
		t.Error(" Delete user FAIL: ", err)
	}
}

