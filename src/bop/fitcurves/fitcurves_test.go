package fitcurves

import (
	"testing"
	"log"
)

func TestFitCurves(t *testing.T) {

	cf := new(CurveFit)
	cf.Input.Name = "Test"
	cf.Input.RawX = []float64{0,4610271,9220542,13830810,27661626}
	cf.Input.RawY = []float64{0,5310285,8659500,10964643,14942246}
	cf.Input.Function = 0
	cf.Initialise()
	cf.Fit()
	log.Println("fitted", cf.Output.FittedPars, cf.Residual(cf.Output.FittedPars), cf.Output.ScaleX, cf.Output.ScaleY)
}