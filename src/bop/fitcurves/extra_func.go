package fitcurves

import(
	"math"
)

func Zeros2d(rows int, cols int) [][]float64 {
	output := make([][]float64, rows)

	for x := 0; x < rows; x++ {
		output[x] = make([]float64, cols)
	}
	return output
}

type Fit_output struct {
	ScaledX    []float64
	ScaledY    []float64
	Name       string
	FittedPars []float64
	ScaleX     float64
	ScaleY     float64
	Residual   float64
}

func (v *CurveFit) CurvesChart(numberIncrements int, optimal_spend []float64, input []Fit_output, fnc []int) ([][]float64, [][]float64) {
	v.Funcs = []func([]float64, float64) float64{v.Logistic, v.Exponential, v.NegativeExponential}
	outputSales := Zeros2d(numberIncrements, len(input))
	outputSpend := Zeros2d(numberIncrements, len(input))

	haf_incr := math.Floor(float64(numberIncrements) / 2)

	for x, c := range input{
		v.Input.Function = fnc[x]
		v.Output.FittedPars = c.FittedPars
		v.Output.Residual = c.Residual
		v.Output.ScaledX = c.ScaledX
		v.Output.ScaledY = c.ScaledY
		v.Output.ScaleX = c.ScaleX
		v.Output.ScaleY = c.ScaleY
		var spends []float64
		for i := 0; i < numberIncrements; i++ {
			spends = append(spends, ( optimal_spend[x] / haf_incr ) * float64(i))
		}
		sales := v.Calculate(spends)
		for i := 0; i < numberIncrements; i++ {
			outputSales[i][x] = sales[i]
			outputSpend[i][x] = spends[i]
		}
	}

	return outputSpend, outputSales
}