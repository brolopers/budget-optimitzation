package fitcurves

/**
 Struct Curve Fitting
 */
type Curvefit_input struct {
	Function 	    int
	Name 			string
	RawX       		[]float64
	RawY       		[]float64
	Raw_spend       float64
}

type Curvefit_output struct {
	ScaledX    []float64
	ScaledY    []float64
	Name       string
	FittedPars []float64
	ScaleX     float64
	ScaleY     float64
	Residual   float64
}

type Curvefit_table struct {
	Name 			string
	Color			string
	Function 	    int
	Visible			int
	Spend			float64
	Sales			float64
	Min				float64
	Max				float64
	Threshold		float64
}