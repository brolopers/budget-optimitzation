package optimisecurves

import(
	"strings"
)


type BudgetOutput1 struct {
	OverallSpend 	float64
	Spends       	[]float64
	OverallSales 	float64
	Sales       	[]float64
}


func Zeros2d(rows int, cols int) [][]float64 {
	output := make([][]float64, rows)

	for x := 0; x < rows; x++ {
		output[x] = make([]float64, cols)
	}
	return output
}

func subArr(a []float64, b []float64) []float64 {
	size := len(a)
	output := make([]float64, size)

	for x :=0; x < size; x++ {
		output[x] = a[x] - b[x]
	}

	return output
}

func max_num(a []float64)(int){
	max := a[0]
	idx := 0
	for x :=1; x < len(a); x++ {
		if max < a[x] {
			max = a[x]
			idx = x
		}
	}
	return idx
}


func (v *OptimiseCurves) ProfitChart(numberIncrements int, max_spend float64, min_spend float64) ([][]float64, [][]float64, []float64, []float64) {
	/**
		Draw Profit Curves
		Notes: In all cases we start from 0 and the profit point is at 7M.
		-Case 0:
		Setup Max of 10 and we have 5 points. This it will be the points in M:
		[0,2,4,6,8,10] How the profit point is at 7 we draw an extra point (The point 7M)

		-Case 1:
		Setup Max of 5 and we have 5 points. This it will be the points in M:
		[0,1,2,3,4,5] How the profit point is at 7 won't we draw the maximum profit point.

		-Case 2:
		Setup Max of -1 (Empty in client side). Find the Max profit point (7M) and split into half points every side.
		Like That:
		[0,3.5,7,10.5,14]

	 */
	if max_spend > 0{//Case 0 and 1
		outputSales, outputSpend, sum_sales, sum_spend := v.CalculateSalesSpendArray(numberIncrements, max_spend, min_spend)

		//Find Max profit point
		profit_arr := subArr(sum_sales, sum_spend)
		idx := max_num(profit_arr)
		if idx < len(outputSpend) {
			pv_sales, pv_spend := v.FindProfitPoints()

			if len(pv_spend) > 0 {
				outputSales, outputSpend = v.InsertPoints(pv_sales, pv_spend, outputSales, outputSpend)
			}
			return outputSpend, outputSales, pv_sales, pv_spend
		}else{
			return outputSpend, outputSales, []float64{}, []float64{}
		}


	}else{//Case 2
		pv_sales, pv_spend := v.FindProfitPoints()
		outputSales, outputSpend, _, _ := v.CalculateSalesSpendArray(numberIncrements, sum(pv_spend) * 2, min_spend)
		outputSales, outputSpend = v.InsertPoints(pv_sales, pv_spend, outputSales, outputSpend)
		return outputSpend, outputSales, pv_sales, pv_spend
	}
}

func (v *OptimiseCurves) FindProfitPoints() ([]float64, []float64){
	var pv_sales []float64
	var pv_spend []float64

	v.Overall = 0.0
	optimalSpend, msg := v.Calculate()
	if strings.ToLower(msg) == "ok" {
		pv_sales = v.SalesCurve(optimalSpend)
		pv_spend = optimalSpend
	}

	return pv_sales, pv_spend
}

func (v *OptimiseCurves) CalculateSalesSpendArray(numberIncrements int, max_spend float64, min_spend float64) ([][]float64, [][]float64, []float64, []float64){
	outputSales := Zeros2d(numberIncrements + 1, len(v.Input.Channels))
	outputSpend := Zeros2d(numberIncrements + 1, len(v.Input.Channels))
	sum_spends := make([]float64, numberIncrements + 1)
	sum_sales := make([]float64, numberIncrements + 1)

	incrementSize := (max_spend - min_spend)/float64(numberIncrements)
	spend := min_spend

	if min_spend > 0.0 {
		v.Overall = spend
		optimalSpends, msg := v.Calculate()
		if strings.ToLower(msg) == "ok" {
			outputSpend[0] = optimalSpends
			outputSales[0] = v.SalesCurve(optimalSpends)
			sum_spends[0] = sum(outputSpend[0])
			sum_sales[0] = sum(outputSales[0])
		}else{
			outputSpend[0][0] = spend
		}
	}
	spend += incrementSize

	for i := 1; i < numberIncrements + 1; i++ {
		v.Overall = spend
		optimalSpends, msg := v.Calculate()
		if strings.ToLower(msg) == "ok" {
			outputSpend[i] = optimalSpends
			outputSales[i] = v.SalesCurve(optimalSpends)
			sum_spends[i] = sum(outputSpend[i])
			sum_sales[i] = sum(outputSales[i])
		}else{
			outputSpend[i][0] = spend
		}
		spend += incrementSize
	}

	return outputSales, outputSpend, sum_sales, sum_spends
}

func (v *OptimiseCurves) InsertPoints(nwp_sales []float64, nwp_spend []float64, sales [][]float64, spend [][]float64) ([][]float64, [][]float64){
	var new_sales [][]float64
	var new_spend [][]float64
	spend_put_in := false
	nw_sum := sum(nwp_spend)

	for i := 0; i < len(spend); i++ {
		if sum(spend[i]) <= nw_sum || spend_put_in{
			new_spend = append(new_spend, spend[i])
			new_sales = append(new_sales, sales[i])
		}else{
			spend_put_in = true
			new_spend = append(new_spend, nwp_spend)
			new_sales = append(new_sales, nwp_sales)
		}
	}

	return new_sales, new_spend
}


func (v *OptimiseCurves) BudgetChart(spend [][]float64, sales [][]float64) ([]BudgetOutput1) {
	output := make([]BudgetOutput1, len(spend))

	for i := 0; i < len(spend); i++ {
		output[i].OverallSpend = sum(spend[i])
		output[i].OverallSales = sum(sales[i])
		output[i].Spends = spend[i]
		output[i].Sales = sales[i]
	}

	return output
}


func (v *OptimiseCurves) SalesCurve(spends []float64) []float64 {
	outputSales := make([]float64, len(spends))
	for i, c := range v.Input.Channels {
		f := v.CurveFuncs[c.Function]
		outputSales[i] = (f(c.FittedPars, spends[i]))
	}
	return outputSales
}