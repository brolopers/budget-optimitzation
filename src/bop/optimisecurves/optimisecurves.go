package optimisecurves

import (
	"fmt"
	"math"
)

type optimiseInput struct {
	Channels []OptimiseChannel
}

type OptimiseChannel struct {
	Function   int
	Name       string
	FittedPars []float64
	Min        float64
	Max        float64
	Threshold  float64
}

type OptimiseCurves struct {
	CurveFuncs []func([]float64, float64) float64
	Overall    float64
	Input      optimiseInput
}

//coupled to curve fit - changes must be made in curvefit dependency ..
//note 4th parameter is scaleX, 5th parameter is scaleY
func (v *OptimiseCurves) Logistic(k []float64, x float64) float64 {
	return k[4] * k[0] / (1 + k[1]*(math.Pow((x+0.001)/k[3], k[2])))
}

func (v *OptimiseCurves) Exponential(k []float64, x float64) float64 {
	return k[4] * k[0] / (1 + k[1]*k[3]/(x+0.001))
}

func (v *OptimiseCurves) NegativeExponential(k []float64, x float64) float64 {
	return k[4] * k[0] * (1 - math.Exp(-k[1]*(x/k[3])))
}

func fmap(f func(float64) float64, data []float64) []float64 {

	output := make([]float64, len(data))
	for i, _ := range data {
		output[i] = f(data[i])
	}
	return output
}

type BudgetOutput struct {
	OverallSpend float64
	Spends       []float64
}

func (v *OptimiseCurves) BudgetCurve(start float64, incrementSize float64, numberIncrements int) []BudgetOutput {

	spend := start

	output := make([]BudgetOutput, numberIncrements)

	for i := 0; i < numberIncrements; i++ {
		v.Overall = spend
		fmt.Println(v.CurveFuncs)
		optimalSpends, _ := v.Calculate()
		output[i].OverallSpend = sum(optimalSpends)
		output[i].Spends = optimalSpends

		spend += incrementSize

	}

	return output

}

func (v *OptimiseCurves) ProfitCurve(start float64, incrementSize float64, numberIncrements int) ([]float64, []float64, []float64) {

	spend := start
	outputSales := make([]float64, numberIncrements)
	outputSpend := make([]float64, numberIncrements)
	outputProfit := make([]float64, numberIncrements)

	for i := 0; i < numberIncrements; i++ {
		v.Overall = spend
		optimalSpends, _ := v.Calculate()
		profit := v.ProfitCalculation(optimalSpends)
		sales := v.SalesCalculation(optimalSpends)

		outputSpend[i] = sum(optimalSpends)
		outputSales[i] = sales
		outputProfit[i] = profit
		spend += incrementSize

	}

	return outputSpend, outputSales, outputProfit

}

func (v *OptimiseCurves) ProfitCalculation(spends []float64) float64 {

	sum := 0.0
	for i, c := range v.Input.Channels {
		f := v.CurveFuncs[c.Function]
		sum += (f(c.FittedPars, spends[i]) - spends[i])
	}
	return sum
}

func (v *OptimiseCurves) SalesCalculation(spends []float64) float64 {

	sum := 0.0
	for i, c := range v.Input.Channels {
		f := v.CurveFuncs[c.Function]
		sum += (f(c.FittedPars, spends[i]))
	}
	return sum
}

func (v *OptimiseCurves) checkForThresholds() bool {

	applyThreshold := false

	for x := 0; x < len(v.Input.Channels); x++ {
		if v.Input.Channels[x].Threshold > 0.0 {
			applyThreshold = true
			break
		}
	}

	return applyThreshold
}

func sum(data []float64) float64 {

	output := 0.0
	for i, _ := range data {
		output += data[i]
	}
	return output
}

func (v *OptimiseCurves) checkFeasibility() (string, bool) {

	success := true
	message := "Ok"

	if v.Overall > 0 {

		sum := 0.0

		for x := 0; x < len(v.Input.Channels); x++ {
			sum += v.Input.Channels[x].Max - v.Input.Channels[x].Min
		}

		if sum < v.Overall {
			message = "Not enough allocation space, check min and max constraints."
			success = false
		}

		sumMins := 0.0

		for x := 0; x < len(v.Input.Channels); x++ {
			sumMins += v.Input.Channels[x].Min
		}

		if sumMins > v.Overall {
			message = "Minimum are too large for overall budget."
			success = false
		}
	}

	return message, success
}

func (v *OptimiseCurves) initialGuess(f func([]float64) float64) []float64 {

	output := make([]float64, len(v.Input.Channels))

	for x, _ := range v.Input.Channels {
		output[x] = v.bisect(x, len(v.Input.Channels), 100000000, v.ProfitCalculation)
	}

	sum := sum(output)
	weight := 1.0
	if sum > 0 {
		weight = v.Overall / sum
	}

	if v.Overall != 0.0 {
		for x, _ := range v.Input.Channels {
			output[x] *= weight
			output[x] = output[x]
		}
	}

	toRemove := 0.0
	for x, _ := range v.Input.Channels {
		if output[x] < v.Input.Channels[x].Min {
			toRemove += math.Abs(output[x] - v.Input.Channels[x].Min)
			output[x] = v.Input.Channels[x].Min
		}

	}

	toAssign := 0.0
	for x, _ := range v.Input.Channels {
		if output[x] > v.Input.Channels[x].Max {
			toAssign += math.Abs(v.Input.Channels[x].Max - output[x])
			output[x] = v.Input.Channels[x].Max
		}

	}

	if v.Overall != 0.0 {

		toAdd := toAssign - toRemove
		sign := -1.0

		if toAdd > 0.0 {
			sign = 1.0
		}

		toAdd = math.Abs(toAdd)

		if sign > 0 {
			output = v.greedyIn(output, toAdd, toAdd/100, v.Input.Channels, f)
		} else {
			output = v.greedyOut(output, toAdd, toAdd/100, v.Input.Channels, f)

		}
	}

	return output

}

func (v *OptimiseCurves) Calculate() ([]float64, string) {

	v.CurveFuncs = []func([]float64, float64) float64{v.Logistic, v.Exponential, v.NegativeExponential}

	applyThreshold := v.checkForThresholds()
	ok := "Ok"
	f := v.ProfitCalculation

	currentSolution := make([]float64, len(v.Input.Channels))

	if message, check := v.checkFeasibility(); !check {
		ok := message
		return currentSolution, ok
	}
	guess := v.initialGuess(f)
	if v.Overall == 0.0 {
		return guess, ok
	}

	calculate := func(initalGuess []float64) {

		copy(currentSolution, initalGuess)

		incsGreedy := []float64{10.0, 100.0, 1000.0}
		greedy := v.greedySolution(incsGreedy, f)

		if f(greedy) > f(currentSolution) {
			copy(currentSolution, greedy)
		}

		incsShuffle := []float64{10.0, 100.0, 500.0, 1000.0, 5000.0, 10000.0, 100000.0}
		shuffleSolution := v.shuffleSolution(currentSolution, incsShuffle, f)

		if f(shuffleSolution) > f(currentSolution) {
			copy(currentSolution, shuffleSolution)
		}

	}

	calculate(guess)
	if applyThreshold {
		mins := make([]float64, len(v.Input.Channels))
		maxs := make([]float64, len(v.Input.Channels))

		for x, _ := range v.Input.Channels {
			mins[x] = v.Input.Channels[x].Min
			maxs[x] = v.Input.Channels[x].Max

			if currentSolution[x] < v.Input.Channels[x].Threshold {

				v.Input.Channels[x].Min = 0.0
				v.Input.Channels[x].Max = 0.0
			}
		}

		currentSolution = make([]float64, len(v.Input.Channels))

		guess := v.initialGuess(f)
		calculate(guess)

		for x, _ := range v.Input.Channels {
			v.Input.Channels[x].Min = mins[x]
			v.Input.Channels[x].Max = maxs[x]
		}
	}

	return currentSolution, ok
}

func (v *OptimiseCurves) greedySolution(incs []float64, f func([]float64) float64) []float64 {

	output := make([]float64, len(v.Input.Channels))

	bestSolution := -1 * math.Pow(100, 100)

	for _, i := range incs {
		increment := math.Pow(10, float64(math.Ceil(math.Log10(v.Overall/i))))

		start := make([]float64, len(v.Input.Channels))
		sumMins := 0.0
		for x, _ := range v.Input.Channels {
			start[x] = v.Input.Channels[x].Min
			sumMins += start[x]
		}

		greedySolution := v.greedyIn(start, v.Overall-sumMins, increment, v.Input.Channels, v.ProfitCalculation)

		if f(greedySolution) > bestSolution {
			copy(output, greedySolution)
		}
	}

	return output
}

func (v *OptimiseCurves) shuffleSolution(currentSolution []float64, incs []float64, f func([]float64) float64) []float64 {

	output := make([]float64, len(v.Input.Channels))
	bestSolution := -1 * math.Pow(100, 100)

	for _, i := range incs {
		increment := math.Pow(10, float64(math.Ceil(math.Log10(v.Overall/i))))
		shuffleSolution := v.shuffle(increment, currentSolution, v.Input.Channels, v.ProfitCalculation)
		if f(shuffleSolution) > bestSolution {
			copy(output, shuffleSolution)
		}
	}

	return output
}

func (v *OptimiseCurves) CalculateBrute() ([]string, []float64) {

	v.CurveFuncs = []func([]float64, float64) float64{v.Logistic, v.Exponential, v.NegativeExponential}

	solution := v.bruteForce(v.Overall, 1000, v.Input.Channels, v.ProfitCalculation)

	return []string{"test", "test2"}, solution
}

func (v *OptimiseCurves) constraintSatisfied(value float64, min float64, max float64) bool {
	return value >= min && value <= max
}

func (v *OptimiseCurves) arange(max float64, inc float64) []float64 {

	increments := int(max / inc)
	output := make([]float64, increments+1)
	for x := 0; x < increments+1; x++ {
		output[x] = float64(x) * float64(inc)
	}
	return output
}

func (v *OptimiseCurves) shuffle(inc float64, output []float64, channels []OptimiseChannel, f func([]float64) float64) []float64 {
	const maxIterations = 100

	for iteration := 0; iteration < maxIterations; iteration++ {

		takeOut := v.findShuffleIndex(inc, output, false, channels, f)
		putIn := -1

		if takeOut != -1 {
			output[takeOut] -= inc
			putIn = v.findShuffleIndex(inc, output, true, channels, f)
			output[takeOut] += inc
		}

		if takeOut != -1 && putIn != -1 {
			output[takeOut] -= inc
			output[putIn] += inc
		} else {
			break
		}

		if iteration > maxIterations {
			break
		}
	}

	return output
}

func (v *OptimiseCurves) findShuffleIndex(inc float64, output []float64, maximize bool, channels []OptimiseChannel, f func([]float64) float64) int {
	best := -1 * math.Pow(10, 10)
	bestIndex := -1

	if maximize {
		inc *= -1

	}
	for x, _ := range channels {

		output[x] -= inc

		test := -1.0
		if v.constraintSatisfied(output[x], channels[x].Min, channels[x].Max) {
			test = f(output)
		} else {

			test = -1 * math.Pow(10, 10)
		}

		if test > best {
			best = test
			bestIndex = x
		}

		output[x] += inc

	}

	return bestIndex
}

func (v *OptimiseCurves) bruteForce(overall float64, inc float64, channels []OptimiseChannel, f func([]float64) float64) []float64 {

	const maxIterations = 1000

	best := -1 * math.Pow(10, 10)

	bestX, bestY, bestZ := -1.0, -1.0, -1.0

	test := 0.0

	output := make([]float64, len(channels))
	budget := v.arange(overall, inc)
	for _, x := range budget {
		for _, y := range budget {
			for _, z := range budget {
				if v.constraintSatisfied(x, channels[0].Min, channels[0].Max) && v.constraintSatisfied(y, channels[1].Min, channels[1].Max) && v.constraintSatisfied(z, channels[2].Min, channels[2].Max) && (x+y+z == overall) {
					output[0] = x
					output[1] = y
					output[2] = z
					test = f(output)
					if test > best {
						best = test

						bestX, bestY, bestZ = x, y, z
					}

				}
			}
		}
	}

	output[0] = bestX
	output[1] = bestY
	output[2] = bestZ
	return output

}

func (v *OptimiseCurves) bisect(id int, size int, upperBound float64, f func([]float64) float64) float64 {

	fDX := upperBound
	fBis := 0.0
	fMid := 0.0
	fMidEval := 0.0
	dx := 0.00001

	values := make([]float64, size)
	valuesInc := make([]float64, size)

	for i := 0; i < 50; i++ {
		fDX *= 0.5
		fMid = fBis + fDX
		values[id] = fMid
		valuesInc[id] = fMid + dx
		fMidEval = -1 * (f(valuesInc) - f(values)) / dx
		if fMidEval < 0.0 {
			fBis = fMid
		}
	}

	return fMid
}

func (v *OptimiseCurves) greedyIn(start []float64, overall float64, inc float64, channels []OptimiseChannel, f func([]float64) float64) []float64 {

	const maxIterations = 1000
	iteration := 0
	size := len(channels)

	output := make([]float64, len(channels))
	copy(output, start)
	for {
		iteration += 1

		if overall <= 0.0 {
			break
		}

		best := -1 * math.Pow(10, 10)
		bestIndex := -1

		for x := 0; x < size; x++ {

			output[x] += inc
			test := -1.0
			if v.constraintSatisfied(output[x], channels[x].Min, channels[x].Max) {
				test = f(output)
			} else {
				test = -1 * math.Pow(10, 10)
			}

			if test > best {
				best = test
				bestIndex = x
			}
			output[x] -= inc
		}

		if bestIndex != -1 {
			output[bestIndex] += inc
			overall -= inc
		}

		if iteration > maxIterations {
			break
		}
	}

	return output

}

func (v *OptimiseCurves) greedyOut(start []float64, overall float64, inc float64, channels []OptimiseChannel, f func([]float64) float64) []float64 {

	const maxIterations = 1000
	iteration := 0
	size := len(channels)

	output := make([]float64, len(channels))
	copy(output, start)
	for {
		iteration += 1

		if overall <= 0.0 {
			break
		}

		best := -1 * math.Pow(10, 10)
		bestIndex := -1

		for x := 0; x < size; x++ {

			output[x] -= inc
			test := -1.0

			if v.constraintSatisfied(output[x], channels[x].Min, channels[x].Max) {
				test = f(output)
			} else {
				test = -1 * math.Pow(10, 10)
			}

			if test > best {
				best = test
				bestIndex = x
			}
			output[x] += inc
		}

		if bestIndex != -1 {
			output[bestIndex] -= inc
			overall -= inc
		}

		if iteration > maxIterations {
			break
		}
	}

	return output

}